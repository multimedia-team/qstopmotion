<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/aboutdialog.cpp" line="+53"/>
        <source>This is the qStopMotion application for creating stop motion animations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+6"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2"/>
        <source>qStopMotion is a fork of stopmotion for linux.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+4"/>
        <source>Main developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A&amp;uthors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Danish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Czech</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&amp;Thanks To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This program is distributed under the terms of the GPL v2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Licence Agreement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Qt runtime version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operating system name and version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Build time: %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;System Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>About</source>
        <translation type="unfinished">Sobre</translation>
    </message>
</context>
<context>
    <name>AnimationProject</name>
    <message>
        <location filename="../src/domain/animation/animationproject.cpp" line="+577"/>
        <source>Saving scenes to disk ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+972"/>
        <location line="+7"/>
        <source>Add Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Cannot open the selected audio file for reading.
Check that you have the right permissions set.
The animation will be run without sound if you
choose to play.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The selected audio file is not valid within the
given audio format. The animation will be run
without sound if you choose to play.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraControllerDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/cameracontrollerdialog.cpp" line="+270"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>qStopMotion Camera Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Video Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Brightness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contrast:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Saturation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Hue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gamma:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Sharpness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sharpness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Backlight Compensation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backlight Compensation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic White Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>White Balance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Color Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Color Enable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Camera Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic Exposure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exposure:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Focus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Pan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Tilt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tilt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Iris:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Roll:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Reset to Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Fechar</translation>
    </message>
    <message>
        <location line="+806"/>
        <source>Restore Camera Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1404"/>
        <source>Reset Camera Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Abort Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ControllerWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/controllerwidget.cpp" line="+104"/>
        <source>Below you can set which features of the camera should be used in the camera control dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Image quality controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Brightness control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Contrast control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Saturation control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hue control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gamma control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sharpness control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backlight compensation control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>White balance control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gain control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Color enable control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Camera controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Exposure control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Focus control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pan control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tilt control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Iris control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Roll control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DescriptionDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/descriptiondialog.cpp" line="+35"/>
        <source>&amp;Project Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Scene Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Take Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Cancelar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Project Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Scene Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Take Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+11"/>
        <location line="+11"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-22"/>
        <source>The character &apos;|&apos; is not allowed in the project description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The character &apos;|&apos; is not allowed in the scene description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The character &apos;|&apos; is not allowed in the take description.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DomainFacade</name>
    <message>
        <location filename="../src/domain/domainfacade.cpp" line="+152"/>
        <location line="+411"/>
        <location line="+775"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1185"/>
        <source>Can&apos;t open history file to write entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+411"/>
        <source>Can&apos;t open history file to recover project!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+775"/>
        <source>Can&apos;t copy image to temp directory!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/exportwidget.cpp" line="+97"/>
        <source>Below you can set which program should be used for encoding a new project to a video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Below you can set which program should be used for encoding the currently active project to a video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Encoder settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Encoder Application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ffmpeg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>libav</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>AVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MP4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Frame Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>QVGA (320x240)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>VGA (640x480)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SVGA (800x600)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PAL D (704x576)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>HD Ready (1280x720)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full HD (1900x1080)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Frames per Second:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;FPS chooser&lt;/h4&gt; &lt;p&gt;By changing the value in this chooser you set which speed the animation in the &lt;b&gt;FrameView&lt;/b&gt; should run at.&lt;/p&gt; &lt;p&gt;To start an animation press the &lt;b&gt;Run Animation&lt;/b&gt; button.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Splitting up the movie on several files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All scenes and takes will be united in one movie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Movie will be splitted up by scenes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Movie will be splitted up by takes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Output file settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Do you want to be asked for an output file everytime you choose to export?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes</source>
        <translation type="unfinished">Sim</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No</source>
        <translation type="unfinished">Não</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set default output file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Browse</source>
        <translation type="unfinished">Navegar</translation>
    </message>
    <message>
        <location line="+342"/>
        <source>Choose output file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Exposure</name>
    <message>
        <location filename="../src/domain/animation/exposure.cpp" line="+199"/>
        <location line="+4"/>
        <location line="+41"/>
        <location line="+57"/>
        <location line="+33"/>
        <location line="+4"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-138"/>
        <location line="+45"/>
        <source>Can&apos;t copy the image to the temporary directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Can&apos;t remove the image from the project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Can&apos;t save the image in the new file format to the temporary directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Can&apos;t copy the image to the project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t remove the image in the temporary directory!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExternalCommandDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/externalcommanddialog.cpp" line="+50"/>
        <source>Input to program:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Close</source>
        <translation type="unfinished">Fechar</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Output from external command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <location line="+4"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Successful!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/generaldialog.cpp" line="+64"/>
        <source>Preferences</source>
        <translation type="unfinished">Preferências</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Apply</source>
        <translation type="unfinished">Aplicar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close</source>
        <translation type="unfinished">Fechar</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>qStopMotion Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>New Project Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Image Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Image Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Video Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Camera Controller</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/generalwidget.cpp" line="+99"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Capture Button Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Insert new frame bevor selected frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Insert new frame after selected frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Append new frame at the end of the take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Grid Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Vertical Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Horizontal Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Grid Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Signal Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take Picture Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Image editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Select application for edit exposures:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pease restart qStopMotion to activate the new style!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+181"/>
        <source>Choose image editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GrabberWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/grabberwidget.cpp" line="+75"/>
        <source>Below you can select which image grabber should be used for grabbing images from the camera. If available the controller can be used to control focus, zoom and other functionality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Grabber Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video 4 Linux 2 Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Camera Controller (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Microsoft Media Foundation Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Gphoto2 Source (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+135"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pease restart qStopMotion to activate the new grabber stettings!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/helpbrowser.cpp" line="+39"/>
        <source>qStopMotion Help Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Fechar</translation>
    </message>
</context>
<context>
    <name>ImageGrabberFacade</name>
    <message>
        <location filename="../src/technical/grabber/imagegrabberfacade.cpp" line="+162"/>
        <source>Check image sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No usable video image source found. Please
connect on video device on the computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Check image grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Image grabber initialization failed. This may happen 
if you try to grab from an invalid device. Please
select another device on the recording tool tab.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/importwidget.cpp" line="+84"/>
        <source>Below you can set which image sources should be used for importing images to a new project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Below you can set which image sources should be used for importing images to the currently active project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Image import settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Image Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>JPEG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TIFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>BMP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Image Quality:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Image Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Default Grabber Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>QVGA (320x240)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>VGA (640x480)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SVGA (800x600)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PAL D (704x576)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>HD Ready (1280x720)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full HD (1900x1080)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Live view settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Frames per second:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <source>Services</source>
        <translation type="vanished">Serviços</translation>
    </message>
    <message>
        <source>Hide %1</source>
        <translation type="vanished">Ocultar %1</translation>
    </message>
    <message>
        <source>Hide Others</source>
        <translation type="vanished">Ocultar Outros</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation type="vanished">Mostrar Tudo</translation>
    </message>
    <message>
        <source>Preferences...</source>
        <translation type="vanished">Preferências…</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="vanished">Encerrar %1</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="vanished">Sobre o %1</translation>
    </message>
</context>
<context>
    <name>MainWindowGUI</name>
    <message>
        <location filename="../src/frontends/qtfrontend/mainwindowgui.cpp" line="+188"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Abrir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Save</source>
        <translation type="unfinished">&amp;Gravar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save &amp;As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cinelerra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Properties</source>
        <translation type="unfinished">Propriedades</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Undo</source>
        <translation type="unfinished">&amp;Desfazer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Re&amp;do</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Preferences</source>
        <translation type="unfinished">Preferências</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Undo stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Camera Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>What&apos;s &amp;This</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+12"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Ajuda</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+212"/>
        <source>Online Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-211"/>
        <location line="+219"/>
        <source>Support Us</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-218"/>
        <location line="+227"/>
        <source>About Qt</source>
        <translation type="unfinished">Acerca do Qt</translation>
    </message>
    <message>
        <location line="-226"/>
        <location line="+236"/>
        <source>About</source>
        <translation type="unfinished">Sobre</translation>
    </message>
    <message>
        <location line="-233"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open &amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Project ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scene ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Take ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exposure ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>&lt;h4&gt;New&lt;/h4&gt; &lt;p&gt;Creates a &lt;em&gt;new&lt;/em&gt; project.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Open&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Opens&lt;/em&gt; a qStopMotion project file.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Open project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Save&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Saves&lt;/em&gt; the current animation as a qStopMotion project file. &lt;BR&gt;If this project has been saved before it will automatically be saved to the previously selected file.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Save As&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Saves&lt;/em&gt; the current animation as a qStopMotion project file.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save project As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Video&lt;/h4&gt; &lt;p&gt;Exports the current project as &lt;em&gt;video&lt;/em&gt;.&lt;/p&gt;You will be given a wizard to guide you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Cinelerra&lt;/h4&gt; &lt;p&gt;Exports the current animation as a &lt;em&gt;cinelerra-cv&lt;/em&gt; project.&lt;/p&gt;You will be given a wizard to guide you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Properties of the project&lt;/h4&gt; &lt;p&gt;This will opens a window where you can &lt;em&gt;change&lt;/em&gt; properties of the animation project.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Properties of the animation project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Quit&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Quits&lt;/em&gt; the program.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Quit</source>
        <translation type="unfinished">Sair</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;h4&gt;Undo&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Undoes&lt;/em&gt; your last operation. You can press undo several time to undo earlier operations.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Undo</source>
        <translation type="unfinished">Desfazer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Redo&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Redoes&lt;/em&gt; your last operation. You can press redo several times to redo several operations.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Redo</source>
        <translation type="unfinished">Refazer</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>&lt;h4&gt;Preferences of the application&lt;/h4&gt; &lt;p&gt;This will opens a window where you can &lt;em&gt;change&lt;/em&gt; the preferences of the application.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Preferences of qStopMotion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;What&apos;s This&lt;/h4&gt; &lt;p&gt;This will give you a WhatsThis mouse cursor which can be used to bring up helpful information like this.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>What&apos;s This</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Help&lt;/h4&gt; &lt;p&gt;This button will bring up a dialog with the qStopMotion manual&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Online Help&lt;/h4&gt; &lt;p&gt;This button will bring up the browser with the qStopMotion manual in different languages&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Support Us&lt;/h4&gt; &lt;p&gt;This button will bring up the browser with the qStopMotion support us page&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;About Qt&lt;/h4&gt; &lt;p&gt;This will display a small information box where you can read general information about the Qt library.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;h4&gt;About&lt;/h4&gt; &lt;p&gt;This will display a small information box where you can read general information as well as the names of the developers behind this excellent piece of software.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&lt;h4&gt;Project ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently active project&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Scene ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently selected scene&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Take ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently selected take&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Exposure ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently selected exposure&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;h4&gt;FrameView&lt;/h4&gt;&lt;p&gt; In this area you can see the selected frame. You can also play animations in this window by pressing the &lt;b&gt;Play&lt;/b&gt; button.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;h4&gt;TimeLine&lt;/h4&gt; &lt;p&gt;In this area you can see the frames and scenes in the animations and build the animation by moving the them around.&lt;/p&gt;&lt;p&gt;You can switch to the next and the previous frame using the &lt;b&gt;arrow buttons&lt;/b&gt; or &lt;b&gt;x&lt;/b&gt; and &lt;b&gt;z&lt;/b&gt;&lt;/p&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+385"/>
        <source>Select image grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-384"/>
        <source>You have to define an image grabber to use.
This can be set in the preferences menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Ready to rumble ;-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+118"/>
        <source>No style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancelar</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Connecting camera...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>You have to define an image grabber to use.
This can be set on the recording tool tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Existing Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There are some images in the open project. Do you want to convert the images to the new file format or quality?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+137"/>
        <source>The Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scene 001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Take 01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>qStopMotion - New Animation Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Choose project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+85"/>
        <source>Project (*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Save As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The character &apos;|&apos; is not allowed in the project file name and will be removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+30"/>
        <location line="+40"/>
        <location line="+36"/>
        <location line="+26"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-132"/>
        <source>The project directory must not contain more than one project file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>No encoder selected for the video export.
This can be set in the properties dialog of the project.
Export to video will not be possible until you
have set an encoder to use!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>AVI Videos (*.avi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>MP4 Videos (*.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No video format selected for the video export.
This can be set in the properties dialog of the project.
Export to video will not be possible until you
have set an video format to use!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Export to video file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>No default output file name defined.
Check your settings in the properties dialo of the project!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The selected encoder is not installed on your computer.
Install the encoder or select another one!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Exporting ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Export to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+173"/>
        <source>qStopMotion - Undo stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+184"/>
        <source>English</source>
        <comment>This should be translated to the name of the language you are translating to, in that language. Example: English = Deutsch (Deutsch is &quot;German&quot; in German)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+808"/>
        <source>Unsaved changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There are unsaved changes. Do you want to save?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PPDOptionsModel</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nome</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Valor</translation>
    </message>
</context>
<context>
    <name>PreferencesTool</name>
    <message>
        <location filename="../src/technical/preferencestool.cpp" line="+117"/>
        <location line="+4"/>
        <source>DOM Parser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Couldn&apos;t open XML file:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Parse error at line %1, column %2:
%3
%4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+432"/>
        <location line="+7"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Can&apos;t remove preferences backup file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Can&apos;t copy preferences file to backup file!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/projectdialog.cpp" line="+62"/>
        <source>Properties</source>
        <translation type="unfinished">Propriedades</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Apply</source>
        <translation type="unfinished">Aplicar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close</source>
        <translation type="unfinished">Fechar</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Animation Project Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Image Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Image Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Video Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectSerializer</name>
    <message>
        <location filename="../src/domain/animation/projectserializer.cpp" line="+75"/>
        <location line="+6"/>
        <source>DOM Parser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Couldn&apos;t open XML file:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Parse error at line %1, column %2:
%3
%4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <location line="+7"/>
        <location line="+4"/>
        <location line="+84"/>
        <location line="+4"/>
        <location line="+6"/>
        <location line="+5"/>
        <location line="+6"/>
        <location line="+4"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-119"/>
        <source>Can&apos;t remove old backup of project file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Can&apos;t copy the project file to backup!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t remove the old project file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Can&apos;t create project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t change permissions of the project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can&apos;t create image directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Can&apos;t change permissions of the image directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can&apos;t create sound directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t change permissions of the sound directory!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectTab</name>
    <message>
        <location filename="../src/frontends/qtfrontend/tooltabs/projecttab.cpp" line="+314"/>
        <source>Project Tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scenes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Insert new Scene (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;scene&lt;/em&gt; and &lt;em&gt;insert&lt;/em&gt; it bevor the selected scene.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Append new Scene (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;scene&lt;/em&gt; and &lt;em&gt;append&lt;/em&gt; it at the end of the animation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Remove Scene (SHIFT+Delete)&lt;/h4&gt; &lt;p&gt;Click this button to &lt;em&gt;remove&lt;/em&gt; the selected &lt;em&gt;scene&lt;/em&gt; from the animation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Takes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Insert new Take (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;take&lt;/em&gt; and &lt;em&gt;insert&lt;/em&gt; it bevor the selected take.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Append new Take (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;take&lt;/em&gt; and &lt;em&gt;append&lt;/em&gt; it to the end of the scene.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Remove Take (SHIFT+Delete)&lt;/h4&gt; &lt;p&gt;Click this button to &lt;em&gt;remove&lt;/em&gt; the selected &lt;em&gt;take&lt;/em&gt; from the animation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Insert Frames (CTRL+F)&lt;/h4&gt; &lt;p&gt;Click on this button to &lt;em&gt;insert frames&lt;/em&gt; bevor the selected frame.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Append Frames (CTRL+F)&lt;/h4&gt; &lt;p&gt;Click on this button to &lt;em&gt;append frames&lt;/em&gt; at the end of the take.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Remove Frame (Delete)&lt;/h4&gt; &lt;p&gt;Click this button to &lt;em&gt;remove&lt;/em&gt; the selected &lt;em&gt;frame&lt;/em&gt; from the take.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Launch Photo Editor&lt;/h4&gt; &lt;p&gt;Click this button to open the active frame in the photo editor&lt;/p&gt; &lt;p&gt;Note that you can also drag images from the frame bar and drop them on the photo editor&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+759"/>
        <location line="+35"/>
        <source>Scene 000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-34"/>
        <location line="+35"/>
        <location line="+95"/>
        <location line="+36"/>
        <source>Take 00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-43"/>
        <source>Insert Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+36"/>
        <location line="+89"/>
        <location line="+38"/>
        <source>No scene selected. Please select a scene in the project tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-127"/>
        <source>Add Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+5"/>
        <source>Insert Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+38"/>
        <source>No take selected. Please select a take in the project tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <location line="+5"/>
        <source>Add Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+7"/>
        <location line="+209"/>
        <location line="+42"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-257"/>
        <source>There is no active frame to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The active frame is corrupt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Choose frames to add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>You do not have any photo editor installed on your system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Failed to start Photo editor!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/projectwidget.cpp" line="+92"/>
        <source>Image grabber settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Video 4 Linux 2 (USB WebCam)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>gphoto (USB Compact Camera)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Media Foundation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Single frame capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time-lapse capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Mix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number of images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;h4&gt;Number of images&lt;/h4&gt; &lt;p&gt;By changing the value in this slidebar you can specify how many images backwards in the animation which should be mixed on top of the camera.&lt;/p&gt; &lt;p&gt;By mixing the previous image(s) onto the camera you can more easily see how the next shot will be in relation to the other, therby making a smoother stop motion animation!&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Time-lapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>seconds before picture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+266"/>
        <source>Seconds between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Minutes between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Hours between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Days between pictures</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <source>%1, %2 not defined</source>
        <translation type="vanished">%1, %2 indefinido</translation>
    </message>
    <message>
        <source>Ambiguous %1 not handled</source>
        <translation type="vanished">%1 ambíguo não tratado</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <source>True</source>
        <translation type="vanished">Verdadeiro</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="vanished">Falso</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Inserir</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Actualizar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Remover</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <source>Copy or Move a File</source>
        <translation type="vanished">Copiar ou Mover um Ficheiro</translation>
    </message>
    <message>
        <source>Read: %1</source>
        <translation type="vanished">Ler: %1</translation>
    </message>
    <message>
        <source>Write: %1</source>
        <translation type="vanished">Escrever: %1</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Todos os Ficheiros (*)</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nome</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Tamanho</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Tipo</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Data</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation type="vanished">Atributos</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
    <message>
        <source>Look &amp;in:</source>
        <translation type="vanished">Ver &amp;em:</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation type="vanished">&amp;Nome do Ficheiro:</translation>
    </message>
    <message>
        <source>File &amp;type:</source>
        <translation type="vanished">&amp;Tipo de Ficheiro:</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Recuar</translation>
    </message>
    <message>
        <source>One directory up</source>
        <translation type="vanished">Pasta Mãe</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation type="vanished">Criar Nova Pasta</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Vista Abreviada</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation type="vanished">Vista Detalhada</translation>
    </message>
    <message>
        <source>Preview File Info</source>
        <translation type="vanished">Antever Informação do Ficheiro</translation>
    </message>
    <message>
        <source>Preview File Contents</source>
        <translation type="vanished">Antever Conteúdo do Ficheiro</translation>
    </message>
    <message>
        <source>Read-write</source>
        <translation type="vanished">Leitura e escrita</translation>
    </message>
    <message>
        <source>Read-only</source>
        <translation type="vanished">Apenas Leitura</translation>
    </message>
    <message>
        <source>Write-only</source>
        <translation type="vanished">Apenas Escrita</translation>
    </message>
    <message>
        <source>Inaccessible</source>
        <translation type="vanished">Inacessível</translation>
    </message>
    <message>
        <source>Symlink to File</source>
        <translation type="vanished">Ligação para Ficheiro</translation>
    </message>
    <message>
        <source>Symlink to Directory</source>
        <translation type="vanished">Ligação para Pasta</translation>
    </message>
    <message>
        <source>Symlink to Special</source>
        <translation type="vanished">Ligação para Especial</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Ficheiro</translation>
    </message>
    <message>
        <source>Dir</source>
        <translation type="vanished">Pasta</translation>
    </message>
    <message>
        <source>Special</source>
        <translation type="vanished">Especial</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="vanished">Guardar Como</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Abrir</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Gravar</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Mudar Nome</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Apagar</translation>
    </message>
    <message>
        <source>R&amp;eload</source>
        <translation type="vanished">&amp;Recarregar</translation>
    </message>
    <message>
        <source>Sort by &amp;Name</source>
        <translation type="vanished">Ordenar pelo &amp;Nome</translation>
    </message>
    <message>
        <source>Sort by &amp;Size</source>
        <translation type="vanished">Ordenar pelo &amp;Tamanho</translation>
    </message>
    <message>
        <source>Sort by &amp;Date</source>
        <translation type="vanished">Ordenar pela &amp;Data</translation>
    </message>
    <message>
        <source>&amp;Unsorted</source>
        <translation type="vanished">Não &amp;Ordenado</translation>
    </message>
    <message>
        <source>Sort</source>
        <translation type="vanished">Ordenar</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation type="vanished">Mostrar ficheiros &amp;escondidos</translation>
    </message>
    <message>
        <source>the file</source>
        <translation type="vanished">o ficheiro</translation>
    </message>
    <message>
        <source>the directory</source>
        <translation type="vanished">a pasta</translation>
    </message>
    <message>
        <source>the symlink</source>
        <translation type="vanished">a ligação</translation>
    </message>
    <message>
        <source>Delete %1</source>
        <translation type="vanished">Apagar %1</translation>
    </message>
    <message>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation type="vanished">&lt;qt&gt;Deseja mesmo apagar %1 &quot;%2&quot;?&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Sim</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Não</translation>
    </message>
    <message>
        <source>New Folder 1</source>
        <translation type="vanished">Nova Pasta 1</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">Nova Pasta</translation>
    </message>
    <message>
        <source>New Folder %1</source>
        <translation type="vanished">Nova Pasta %1</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation type="vanished">Procurar Pasta</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Pastas</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation type="vanished">Pasta:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erro</translation>
    </message>
    <message>
        <source>%1
File not found.
Check path and filename.</source>
        <translation type="vanished">%1
Ficheiro não encontrado.
Verifique a localização e o nome.</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Todos os Ficheiros (*.*)</translation>
    </message>
    <message>
        <source>Open </source>
        <translation type="vanished">Abrir </translation>
    </message>
    <message>
        <source>Select a Directory</source>
        <translation type="vanished">Seleccione uma Pasta</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <source>Could not read directory
%1</source>
        <translation type="vanished">Não foi possível ler a pasta
%1</translation>
    </message>
    <message>
        <source>Could not create directory
%1</source>
        <translation type="vanished">Não foi possível criar a pasta
%1</translation>
    </message>
    <message>
        <source>Could not remove file or directory
%1</source>
        <translation type="vanished">Não foi possível apagar o ficheiro ou a pasta
%1</translation>
    </message>
    <message>
        <source>Could not rename
%1
to
%2</source>
        <translation type="vanished">Não foi  possível mudar o nome
%1
para
%2</translation>
    </message>
    <message>
        <source>Could not open
%1</source>
        <translation type="vanished">Não foi possível abrir
%1</translation>
    </message>
    <message>
        <source>Could not write
%1</source>
        <translation type="vanished">Nao foi possível escrever
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <source>Line up</source>
        <translation type="vanished">Alinhar</translation>
    </message>
    <message>
        <source>Customize...</source>
        <translation type="vanished">Configurar...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <source>Operation stopped by the user</source>
        <translation type="vanished">Operação interrompida pelo utilizador</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Aplicar</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ajuda</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="vanished">Predefinições</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Desfazer</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Refazer</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Cor&amp;tar</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Copiar</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">Co&amp;lar</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Limpar</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Seleccionar Tudo</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <source>System</source>
        <translation type="vanished">Sistema</translation>
    </message>
    <message>
        <source>Restore up</source>
        <translation type="vanished">Restaurar acima</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Minimizar</translation>
    </message>
    <message>
        <source>Restore down</source>
        <translation type="vanished">Restaurar abaixo</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Maximizar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Fechar</translation>
    </message>
    <message>
        <source>Contains commands to manipulate the window</source>
        <translation type="vanished">Contém comandos para manipular a janela</translation>
    </message>
    <message>
        <source>Puts a minimized back to normal</source>
        <translation type="vanished">Coloca uma janela minimizada no estado normal</translation>
    </message>
    <message>
        <source>Moves the window out of the way</source>
        <translation type="vanished">Tira a janela da frente</translation>
    </message>
    <message>
        <source>Puts a maximized window back to normal</source>
        <translation type="vanished">Coloca uma janela maximizada no estado normal</translation>
    </message>
    <message>
        <source>Makes the window full screen</source>
        <translation type="vanished">Coloca a janela em ecrã completo</translation>
    </message>
    <message>
        <source>Closes the window</source>
        <translation type="vanished">Fecha a janela</translation>
    </message>
    <message>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation type="vanished">Mostra o nome da janela e contém controlos para a manipular</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <source>More...</source>
        <translation type="vanished">Mais...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <source>The protocol `%1&apos; is not supported</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não é suportado</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta listagem de pastas</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta criação de novas pastas</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta eliminação de ficheiros ou pastas</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta mudança de nome de ficheiros ou pastas</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta obtenção de ficheiros</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta colocação de ficheiros</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation type="vanished">O protocolo &apos;%1&apos; não suporta copiar ou mover ficheiros ou pastas</translation>
    </message>
    <message>
        <source>(unknown)</source>
        <translation type="vanished">(desconhecido)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Cancelar</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation type="vanished">&lt; &amp;Recuar</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="vanished">&amp;Avançar &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation type="vanished">&amp;Terminar</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Ajuda</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Máquina desconhecida</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Ligação recusada</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="obsolete">Ligação expirada</translation>
    </message>
    <message>
        <source>Socket operation timed out</source>
        <translation type="vanished">Operação de &apos;socket&apos; expirada</translation>
    </message>
    <message>
        <source>Socket is not connected</source>
        <translation type="vanished">&apos;Socket&apos; desligado</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="obsolete">Rede inalcançável</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <source>&amp;Step up</source>
        <translation type="vanished">&amp;Passo acima</translation>
    </message>
    <message>
        <source>Step &amp;down</source>
        <translation type="vanished">Passo &amp;abaixo</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Activate</source>
        <translation type="vanished">Activar</translation>
    </message>
    <message>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="vanished">O executável &apos;%1&apos; requere Qt %2, Qt %3 encontrado.</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation type="vanished">Erro de Incompatibilidade da Biblioteca Qt</translation>
    </message>
    <message>
        <source>Activates the program&apos;s main window</source>
        <translation type="vanished">Activa a janela principal do programa</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <source>Select ActiveX Control</source>
        <translation type="vanished">Seleccionar Controlo ActiveX</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Cancelar</translation>
    </message>
    <message>
        <source>COM &amp;Object:</source>
        <translation type="vanished">&amp;Objecto COM:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <source>Uncheck</source>
        <translation type="vanished">Desactivar</translation>
    </message>
    <message>
        <source>Check</source>
        <translation type="vanished">Activar</translation>
    </message>
    <message>
        <source>Toggle</source>
        <translation type="vanished">Comutar</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <source>Hu&amp;e:</source>
        <translation type="vanished">C&amp;or:</translation>
    </message>
    <message>
        <source>&amp;Sat:</source>
        <translation type="vanished">&amp;Saturação:</translation>
    </message>
    <message>
        <source>&amp;Val:</source>
        <translation type="vanished">&amp;Valor:</translation>
    </message>
    <message>
        <source>&amp;Red:</source>
        <translation type="vanished">&amp;Vermelho:</translation>
    </message>
    <message>
        <source>&amp;Green:</source>
        <translation type="vanished">V&amp;erde:</translation>
    </message>
    <message>
        <source>Bl&amp;ue:</source>
        <translation type="vanished">&amp;Azul:</translation>
    </message>
    <message>
        <source>A&amp;lpha channel:</source>
        <translation type="vanished">Canal &amp;transparência:</translation>
    </message>
    <message>
        <source>&amp;Basic colors</source>
        <translation type="vanished">Cores &amp;básicas</translation>
    </message>
    <message>
        <source>&amp;Custom colors</source>
        <translation type="vanished">Cores c&amp;ustomizadas</translation>
    </message>
    <message>
        <source>&amp;Define Custom Colors &gt;&gt;</source>
        <translation type="obsolete">&amp;Definir Cores Customizadas &gt;&gt;</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <source>&amp;Add to Custom Colors</source>
        <translation type="vanished">&amp;Adicionar às Cores Customizadas</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">Seleccionar cor</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="obsolete">Falso</translation>
    </message>
    <message>
        <source>True</source>
        <translation type="obsolete">Verdadeiro</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fechar</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Ligação não possível</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Finalização de transacção não possível</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Anulação de transacção não possível</translation>
    </message>
    <message>
        <source>Unable to set autocommit</source>
        <translation type="vanished">Finalização automática não possível</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Execução não possível</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Preparação não possível</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation type="vanished">Ligação de variável não possível</translation>
    </message>
    <message>
        <source>Unable to fetch record %1</source>
        <translation type="vanished">Obtenção do registo %1 não possível</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation type="vanished">Obtenção do seguinte não possível</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="vanished">Obtenção do primeiro não possível</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <source>AM</source>
        <translation type="vanished">AM</translation>
    </message>
    <message>
        <source>am</source>
        <translation type="vanished">am</translation>
    </message>
    <message>
        <source>PM</source>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>pm</source>
        <translation type="vanished">pm</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <source>What&apos;s This?</source>
        <translation type="vanished">O Que é Isto?</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Gravar</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="obsolete">&amp;Gravar</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fechar</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="obsolete">&amp;Fechar</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Aplicar</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Restaurar</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ajuda</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation type="vanished">Não Gravar</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="vanished">Descartar</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Sim</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation type="vanished">Sim para &amp;Todos</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Não</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation type="vanished">N&amp;ão para Todos</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation type="vanished">Gravar Todos</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="vanished">Abortar</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Tentar Novamente</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignorar</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation type="vanished">Restaurar Predefinições</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation type="vanished">Fechar sem Gravar</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <source>Name</source>
        <translation type="vanished">Nome</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Tamanho</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="vanished">Tipo</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="vanished">Tipo</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation type="vanished">Data de Modificação</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fechar</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <source>More</source>
        <translation type="obsolete">Mais</translation>
    </message>
    <message>
        <source>Less</source>
        <translation type="obsolete">Menos</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <source>Debug Message:</source>
        <translation type="vanished">Mensagem Depuração:</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="vanished">Aviso:</translation>
    </message>
    <message>
        <source>Fatal Error:</source>
        <translation type="vanished">Erro Fatal:</translation>
    </message>
    <message>
        <source>&amp;Show this message again</source>
        <translation type="vanished">&amp;Mostrar esta mensagem novamente</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Todos os Ficheiros (*)</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Pastas</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Abrir</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Gravar</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation type="vanished">%1 já existe.
Deseja substituir?</translation>
    </message>
    <message>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation type="vanished">%1
Ficheiro não encontrado.
Por favor verifique o nome do ficheiro.</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="vanished">O Meu Computador</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Mudar o Nome</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Apagar</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation type="vanished">Mostrar ficheiros &amp;escondidos</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Recuar</translation>
    </message>
    <message>
        <source>Parent Directory</source>
        <translation type="vanished">Pasta Mãe</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Vista Abreviada</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation type="vanished">Vista Detalhada</translation>
    </message>
    <message>
        <source>Files of type:</source>
        <translation type="vanished">FIcheiros do tipo:</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation type="vanished">Pasta:</translation>
    </message>
    <message>
        <source>
File not found.
Please verify the correct file name was given</source>
        <translation type="obsolete">
Ficheiro não encontrado.
Por favor verifique o nome do ficheiro</translation>
    </message>
    <message>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation type="vanished">%1
Pasta não encontrada.
Por favor verifique o nome da pasta.</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation type="vanished">&apos;%1&apos; está protegido contra escrita.
Deseja apagar de qualquer forma?</translation>
    </message>
    <message>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation type="vanished">Deseja mesmo apagar &apos;%1&apos;?</translation>
    </message>
    <message>
        <source>Could not delete directory.</source>
        <translation type="vanished">Não foi possível apagar a pasta.</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Todos os Ficheiros (*.*)</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="vanished">Gravar Como</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation type="vanished">Unidade</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Ficheiro</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Desconhecido</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation type="obsolete">Procurar Pasta</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Seguinte</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="obsolete">Nova Pasta</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation type="obsolete">&amp;Nome do Ficheiro:</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation type="obsolete">Criar Nova Pasta</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nome</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Tamanho</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="obsolete">Tipo</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="obsolete">Tipo</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation type="obsolete">Data de Modificação</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="obsolete">O Meu Computador</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <source>&amp;Font</source>
        <translation type="vanished">&amp;Tipo de Letra</translation>
    </message>
    <message>
        <source>Font st&amp;yle</source>
        <translation type="vanished">&amp;Estilo Tipo de Letra</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Tamanho</translation>
    </message>
    <message>
        <source>Effects</source>
        <translation type="vanished">Efeitos</translation>
    </message>
    <message>
        <source>Stri&amp;keout</source>
        <translation type="vanished">&amp;Riscar</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation type="vanished">&amp;Sublinhar</translation>
    </message>
    <message>
        <source>Sample</source>
        <translation type="vanished">Amostra</translation>
    </message>
    <message>
        <source>Wr&amp;iting System</source>
        <translation type="vanished">&amp;Sistema de Escrita</translation>
    </message>
    <message>
        <source>Select Font</source>
        <translation type="vanished">Seleccione Tipo de Letra</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <source>Not connected</source>
        <translation type="vanished">Desligado</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation type="vanished">Servidor %1 não encontrado</translation>
    </message>
    <message>
        <source>Connection refused to host %1</source>
        <translation type="vanished">Ligação ao servidor %1 recusada</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation type="vanished">Ligado ao servidor %1</translation>
    </message>
    <message>
        <source>Connection refused for data connection</source>
        <translation type="vanished">Ligação de dados recusada</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
    <message>
        <source>Connecting to host failed:
%1</source>
        <translation type="vanished">A ligação ao servidor falhou:
%1</translation>
    </message>
    <message>
        <source>Login failed:
%1</source>
        <translation type="vanished">A autenticação falhou:
%1</translation>
    </message>
    <message>
        <source>Listing directory failed:
%1</source>
        <translation type="vanished">A listagem da pasta falhou:
%1</translation>
    </message>
    <message>
        <source>Changing directory failed:
%1</source>
        <translation type="vanished">A mudança de pasta falhou:
%1</translation>
    </message>
    <message>
        <source>Downloading file failed:
%1</source>
        <translation type="vanished">A descarga do ficheiro falhou:
%1</translation>
    </message>
    <message>
        <source>Uploading file failed:
%1</source>
        <translation type="vanished">O carregamento do ficheiro falhou:
%1</translation>
    </message>
    <message>
        <source>Removing file failed:
%1</source>
        <translation type="vanished">A remoção do ficheiro falhou:
%1</translation>
    </message>
    <message>
        <source>Creating directory failed:
%1</source>
        <translation type="vanished">A criação da pasta falhou:
%1</translation>
    </message>
    <message>
        <source>Removing directory failed:
%1</source>
        <translation type="vanished">A remoção da pasta falhou:
%1</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation type="vanished">Ligação fechada</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation type="vanished">Servidor %1 encontrado</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation type="vanished">Ligação a %1 fechada</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation type="vanished">Servidor encontrado</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation type="vanished">Ligado ao servidor</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation type="vanished">LTR</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Servidor Não encontrado</translation>
    </message>
    <message>
        <source>Unknown address type</source>
        <translation type="vanished">Tipo de endereço desconhecido</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
    <message>
        <source>Request aborted</source>
        <translation type="vanished">Pedido abortado</translation>
    </message>
    <message>
        <source>No server set to connect to</source>
        <translation type="vanished">Nenhum servidor para ligar</translation>
    </message>
    <message>
        <source>Wrong content length</source>
        <translation type="vanished">Tamanho de conteúdo errado</translation>
    </message>
    <message>
        <source>Server closed connection unexpectedly</source>
        <translation type="vanished">O servidor fechou a ligação inesperadamente</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Ligação recusada</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation type="vanished">Servidor %1 não encontrado</translation>
    </message>
    <message>
        <source>HTTP request failed</source>
        <translation type="vanished">O pedido HTTP falhou</translation>
    </message>
    <message>
        <source>Invalid HTTP response header</source>
        <translation type="vanished">Cabeçalho de resposta HTTP inválido</translation>
    </message>
    <message>
        <source>Invalid HTTP chunked body</source>
        <translation type="vanished">Corpo parcial HTTP inválido</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation type="vanished">Servidor %1 encontrado</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation type="vanished">Ligado ao servidor %1</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation type="vanished">Ligação a %1 fechada</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation type="vanished">Servidor encontrado</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation type="vanished">Ligado ao servidor</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation type="vanished">Ligação fechada</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Erro ao abrir a base de dados</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation type="vanished">Não foi possível iniciar a transacção</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Não foi possível anular a transacção</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <source>Unable to create BLOB</source>
        <translation type="vanished">Não foi possível criar o BLOB</translation>
    </message>
    <message>
        <source>Unable to write BLOB</source>
        <translation type="vanished">Não foi possível escrever o BLOB</translation>
    </message>
    <message>
        <source>Unable to open BLOB</source>
        <translation type="vanished">Não foi possível abrir o BLOB</translation>
    </message>
    <message>
        <source>Unable to read BLOB</source>
        <translation type="vanished">Não foi possível ler o BLOB</translation>
    </message>
    <message>
        <source>Could not find array</source>
        <translation type="vanished">Não foi possível encontrar o array</translation>
    </message>
    <message>
        <source>Could not get array data</source>
        <translation type="vanished">Não foi possível obter os dados do array</translation>
    </message>
    <message>
        <source>Could not get query info</source>
        <translation type="vanished">Não foi possível obter informação da query</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation type="vanished">Não foi possível iniciar a transacção</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Could not allocate statement</source>
        <translation type="vanished">Não foi possível alocar a expressão</translation>
    </message>
    <message>
        <source>Could not prepare statement</source>
        <translation type="vanished">Não foi possível preparar a expressão</translation>
    </message>
    <message>
        <source>Could not describe input statement</source>
        <translation type="vanished">Não foi possível descrever a expressão de entrada</translation>
    </message>
    <message>
        <source>Could not describe statement</source>
        <translation type="vanished">Não foi possível descrever a expressão</translation>
    </message>
    <message>
        <source>Unable to close statement</source>
        <translation type="vanished">Não foi possível fechar a expressão</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation type="vanished">Não foi possível executar a query</translation>
    </message>
    <message>
        <source>Could not fetch next item</source>
        <translation type="vanished">Não foi possível obter o elemento seguinte</translation>
    </message>
    <message>
        <source>Could not get statement info</source>
        <translation type="vanished">Não foi possível obter informação da expressão</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Permissão negada</translation>
    </message>
    <message>
        <source>Too many open files</source>
        <translation type="vanished">Demasiados ficheiros abertos</translation>
    </message>
    <message>
        <source>No such file or directory</source>
        <translation type="vanished">Ficheiro ou pasta inexistente</translation>
    </message>
    <message>
        <source>No space left on device</source>
        <translation type="vanished">Dispositivo sem espaço livre</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <source>XIM</source>
        <translation type="vanished">XIM</translation>
    </message>
    <message>
        <source>XIM input method</source>
        <translation type="vanished">Método de entrada XIM</translation>
    </message>
    <message>
        <source>Windows input method</source>
        <translation type="vanished">Método de entrada Windows</translation>
    </message>
    <message>
        <source>Mac OS X input method</source>
        <translation type="vanished">Método de entrada Max OS X</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys: Não foi possível carregar %1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys: Não foi possível descarregar %1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: Símbolo &quot;%1&quot; indefinido em %2 (%3)</translation>
    </message>
    <message>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation type="vanished">Não foi possivel mapear &apos;%1&apos;: %2</translation>
    </message>
    <message>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation type="vanished">Dados de verificação do plugin incorrectos em &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation type="vanished">Não foi possível desmapear &apos;%1&apos;: %2</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation type="vanished">O plugin  &apos;%1&apos; usa uma biblioteca Qt incompatível. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation type="vanished">O plugin &apos;%1&apos; usa uma biblioteca Qt incompatível. A chave de compilação esperada &quot;%2&quot;, ficou  &quot;%3&quot;</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Desfazer</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Refazer</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Cor&amp;tar</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Copiar</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">Co&amp;lar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Apagar</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Seleccionar Tudo</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <source>Unable to open database &apos;</source>
        <translation type="vanished">Não foi possível abrir a base de dados &apos;</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Não foi possível estabelecer a ligação</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Não foi possível iniciar a transacção</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Não foi possível anular a transacção</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <source>Unable to fetch data</source>
        <translation type="vanished">Não foi possível obter dados</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation type="vanished">Não foi possível executar a query</translation>
    </message>
    <message>
        <source>Unable to store result</source>
        <translation type="vanished">Não foi possível guardar o resultado</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Não foi possível preparar a expressão</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Não foi possível restaurar a expressão</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation type="vanished">Não foi possível fazer a ligação do valor</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Não foi possível executar a expressão</translation>
    </message>
    <message>
        <source>Unable to bind outvalues</source>
        <translation type="vanished">Não foi possível fazer a ligação dos valores externos</translation>
    </message>
    <message>
        <source>Unable to store statement results</source>
        <translation type="vanished">Não foi possível guardar os resultados da expressão</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <source>%1 - [%2]</source>
        <translation type="obsolete">%1 - [%2]</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fechar</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="obsolete">Minimizar</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation type="obsolete">Restaurar Baixo</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="obsolete">&amp;Restaurar</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="obsolete">&amp;Mover</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="obsolete">&amp;Tamanho</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="obsolete">Mi&amp;nimizar</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="obsolete">Ma&amp;ximizar</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation type="obsolete">Permanecer no &amp;Topo</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="obsolete">&amp;Fechar</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="obsolete">Maximizar</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ajuda</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="obsolete">Menu</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Fechar</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation type="vanished">Executar</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">Sobre</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">Configurar</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">Preferências</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Opções</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">Alteração</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">Configuração</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Sair</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Sair</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">Sobre %1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">Acerca do Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">Preferências</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">Sair de %1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>Help</source>
        <translation type="vanished">Ajuda</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">Acerca do Qt</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Este programa usa Qt versão %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qtopia Core.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/qt/&quot;&gt;qt.nokia.com/qt/&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;Acerca do Qt&lt;/h3&gt;%1&lt;p&gt;Qt é um conjunto de ferramentas para desenvolvimento de aplicações multiplataforma.&lt;/p&gt;O Qt oferece portabilidade de código fonte único em MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux e todas as principais variantes comerciais de Unix. O Qt está igualmente disponível para dispositivos embebidos como Qtopia Core.&lt;/p&gt;&lt;p&gt;O Qt é um produto Trolltech. Veja &lt;a href=&quot;http://qt.nokia.com/qt/&quot;&gt;qt.nokia.com/qt/&lt;/a&gt; para mais informação.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Show Details...</source>
        <translation type="vanished">Mostrar Detalhes...</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation type="vanished">Não Mostrar Detalhes...</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Este programa usa Qt Open Source Edition versão %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition é indicado para o desenvolvimento de aplicações/programas open source. Se pretender desenvolver aplicações sem disponibilizar o codigo fonte, então precisará de obter uma licença comercial.&lt;/p&gt;&lt;p&gt;Por favor consulte &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;para obter mais informação acerca de licenças Qt.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <source>Select IM</source>
        <translation type="vanished">Seleccione Método de Entrada</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <source>Multiple input method switcher</source>
        <translation type="vanished">Seleccionador de método de entrada</translation>
    </message>
    <message>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation type="vanished">Seleccionador de método de entrada que utiliza o menu de contexto dos elementos de texto</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <source>The remote host closed the connection</source>
        <translation type="vanished">A máquina remota fechou a ligação</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="vanished">Operação de rede expirada</translation>
    </message>
    <message>
        <source>Out of resources</source>
        <translation type="vanished">Sem recursos</translation>
    </message>
    <message>
        <source>Unsupported socket operation</source>
        <translation type="vanished">Operação de &apos;socket&apos; não suportada</translation>
    </message>
    <message>
        <source>Protocol type not supported</source>
        <translation type="vanished">Tipo de protocolo não suportado</translation>
    </message>
    <message>
        <source>Invalid socket descriptor</source>
        <translation type="vanished">Descritor de &apos;socket&apos; inválido</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="vanished">Rede inalcançável</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Permissão negada</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="vanished">Ligação expirada</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Ligação recusada</translation>
    </message>
    <message>
        <source>The bound address is already in use</source>
        <translation type="vanished">O endereço de ligação já está em uso</translation>
    </message>
    <message>
        <source>The address is not available</source>
        <translation type="vanished">O endereço não está disponível</translation>
    </message>
    <message>
        <source>The address is protected</source>
        <translation type="vanished">O endereço está protegido</translation>
    </message>
    <message>
        <source>Unable to send a message</source>
        <translation type="vanished">Não foi possível enviar uma mensagem</translation>
    </message>
    <message>
        <source>Unable to receive a message</source>
        <translation type="vanished">Não foi possível receber uma mensagem</translation>
    </message>
    <message>
        <source>Unable to write</source>
        <translation type="vanished">Não foi possível escrever</translation>
    </message>
    <message>
        <source>Network error</source>
        <translation type="vanished">Erro de rede</translation>
    </message>
    <message>
        <source>Another socket is already listening on the same port</source>
        <translation type="vanished">Outro &apos;socket&apos; já está à escuta no mesmo porto</translation>
    </message>
    <message>
        <source>Unable to initialize non-blocking socket</source>
        <translation type="vanished">Não foi possível inicializar &apos;socket&apos; não bloqueante</translation>
    </message>
    <message>
        <source>Unable to initialize broadcast socket</source>
        <translation type="vanished">Não foi possível inicializar &apos;socket&apos; de transmissão</translation>
    </message>
    <message>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation type="vanished">Tentativa de utilização de &apos;socket&apos; IPv6 numa plataforma sem suporte IPv6</translation>
    </message>
    <message>
        <source>Host unreachable</source>
        <translation type="vanished">Máquina inalcançável</translation>
    </message>
    <message>
        <source>Datagram was too large to send</source>
        <translation type="vanished">Datagrama demasiado grande para enviar</translation>
    </message>
    <message>
        <source>Operation on non-socket</source>
        <translation type="vanished">Operação em não &apos;socket&apos;</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <source>Unable to logon</source>
        <translation type="vanished">Não foi possível autenticar</translation>
    </message>
    <message>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation type="vanished">Não foi possível inicializar</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="obsolete">Não foi possível iniciar a transacção</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <source>Unable to bind column for batch execute</source>
        <translation type="vanished">Não foi possível fazer a licação da coluna para execução &apos;batch&apos;</translation>
    </message>
    <message>
        <source>Unable to execute batch statement</source>
        <translation type="vanished">Não foi possível executar a expressão de &apos;batch&apos;</translation>
    </message>
    <message>
        <source>Unable to goto next</source>
        <translation type="vanished">Não foi possível passar ao seguinte</translation>
    </message>
    <message>
        <source>Unable to alloc statement</source>
        <translation type="vanished">Não foi possível alocar a expressão</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Não foi possível preparar a expressão</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation type="vanished">Não foi possível fazer o ligamento do valor</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">Não foi possível executar a expressão select</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Não foi possível executar a expressão</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Não foi possível ligar</translation>
    </message>
    <message>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation type="vanished">Não foi possível ligar - O &apos;driver&apos; não suporta todas as funcionalidades necessárias</translation>
    </message>
    <message>
        <source>Unable to disable autocommit</source>
        <translation type="vanished">Não foi possível  desactivar finalização automática</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Não foi possível anular a transacção</translation>
    </message>
    <message>
        <source>Unable to enable autocommit</source>
        <translation type="vanished">Não foi possível activar finalização automática</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation type="vanished">QODBCResult::reset: Não foi possível definir &apos;SQL_CURSOR_STATIC&apos; como atributo da expressão. Por favor verifique a configuração do seu &apos;driver&apos; ODBC</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Não foi possível executar a expressão</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation type="vanished">Não foi possível obter o seguinte</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Não foi possível preparar a expressão</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation type="vanished">Não foi possível fazer o ligamento da variável</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="obsolete">Obtenção do primeiro não possível</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Home</source>
        <translation type="vanished">Início</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nome</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Valor</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Não foi possível ligar</translation>
    </message>
    <message>
        <source>Could not begin transaction</source>
        <translation type="vanished">Não foi possível iniciar a transacção</translation>
    </message>
    <message>
        <source>Could not commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Could not rollback transaction</source>
        <translation type="vanished">Não foi possível anular a transacção</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <source>Unable to create query</source>
        <translation type="vanished">Não foi possível criar a &apos;query&apos;</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <source>Page size:</source>
        <translation type="obsolete">Tamanho página:</translation>
    </message>
    <message>
        <source>Paper source:</source>
        <translation type="obsolete">Fonte papel:</translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="obsolete">Retrato</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="obsolete">Paisagem</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>locally connected</source>
        <translation type="vanished">ligado localmente</translation>
    </message>
    <message>
        <source>Aliases: %1</source>
        <translation type="vanished">Nomes Alternativos: %1</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">desconhecido</translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="obsolete">Retrato</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="obsolete">Paisagem</translation>
    </message>
    <message>
        <source>A0 (841 x 1189 mm)</source>
        <translation type="vanished">A0 (841 x 1189 mm)</translation>
    </message>
    <message>
        <source>A1 (594 x 841 mm)</source>
        <translation type="vanished">A1 (594 x 841 mm)</translation>
    </message>
    <message>
        <source>A2 (420 x 594 mm)</source>
        <translation type="vanished">A2 (420 x 594 mm)</translation>
    </message>
    <message>
        <source>A3 (297 x 420 mm)</source>
        <translation type="vanished">A3 (297 x 420 mm)</translation>
    </message>
    <message>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation type="vanished">A4 (210 x 297 mm, 8.26 x 11.7 polegadas)</translation>
    </message>
    <message>
        <source>A5 (148 x 210 mm)</source>
        <translation type="vanished">A5 (148 x 210 mm)</translation>
    </message>
    <message>
        <source>A6 (105 x 148 mm)</source>
        <translation type="vanished">A6 (105 x 148 mm)</translation>
    </message>
    <message>
        <source>A7 (74 x 105 mm)</source>
        <translation type="vanished">A7 (74 x 105 mm)</translation>
    </message>
    <message>
        <source>A8 (52 x 74 mm)</source>
        <translation type="vanished">A8 (52 x 74 mm)</translation>
    </message>
    <message>
        <source>A9 (37 x 52 mm)</source>
        <translation type="vanished">A9 (37 x 52 mm)</translation>
    </message>
    <message>
        <source>B0 (1000 x 1414 mm)</source>
        <translation type="vanished">B0 (1000 x 1414 mm)</translation>
    </message>
    <message>
        <source>B1 (707 x 1000 mm)</source>
        <translation type="vanished">B1 (707 x 1000 mm)</translation>
    </message>
    <message>
        <source>B2 (500 x 707 mm)</source>
        <translation type="vanished">B2 (500 x 707 mm)</translation>
    </message>
    <message>
        <source>B3 (353 x 500 mm)</source>
        <translation type="vanished">B3 (353 x 500 mm)</translation>
    </message>
    <message>
        <source>B4 (250 x 353 mm)</source>
        <translation type="vanished">B4 (250 x 353 mm)</translation>
    </message>
    <message>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation type="vanished">B5 (176 x 250 mm, 6.93 x 9.84 polegadas)</translation>
    </message>
    <message>
        <source>B6 (125 x 176 mm)</source>
        <translation type="vanished">B6 (125 x 176 mm)</translation>
    </message>
    <message>
        <source>B7 (88 x 125 mm)</source>
        <translation type="vanished">B7 (88 x 125 mm)</translation>
    </message>
    <message>
        <source>B8 (62 x 88 mm)</source>
        <translation type="vanished">B8 (62 x 88 mm)</translation>
    </message>
    <message>
        <source>B9 (44 x 62 mm)</source>
        <translation type="vanished">B9 (44 x 62 mm)</translation>
    </message>
    <message>
        <source>B10 (31 x 44 mm)</source>
        <translation type="vanished">B10 (31 x 44 mm)</translation>
    </message>
    <message>
        <source>C5E (163 x 229 mm)</source>
        <translation type="vanished">C5E (163 x 229 mm)</translation>
    </message>
    <message>
        <source>DLE (110 x 220 mm)</source>
        <translation type="vanished">DLE (110 x 220 mm)</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation type="vanished">Executivo (7.5 x 10 polegadas, 191 x 254 mm)</translation>
    </message>
    <message>
        <source>Folio (210 x 330 mm)</source>
        <translation type="vanished">Folio (210 x 330 mm)</translation>
    </message>
    <message>
        <source>Ledger (432 x 279 mm)</source>
        <translation type="vanished">Ledger (432 x 279 mm)</translation>
    </message>
    <message>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation type="vanished">Legal (8.5 x 14 polegadas, 216 x 356 mm)</translation>
    </message>
    <message>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation type="vanished">Carta (8.5 x 11 polegadas, 216 x 279 mm)</translation>
    </message>
    <message>
        <source>Tabloid (279 x 432 mm)</source>
        <translation type="vanished">Tablóide (279 x 432 mm)</translation>
    </message>
    <message>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation type="vanished">Envelope #10 Comum EUA (105 x 241 mm)</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <source>Page size:</source>
        <translation type="obsolete">Tamanho página:</translation>
    </message>
    <message>
        <source>Orientation:</source>
        <translation type="obsolete">Orientação:</translation>
    </message>
    <message>
        <source>Paper source:</source>
        <translation type="obsolete">Fonte papel:</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Imprimir</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Ficheiro</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="obsolete">Impressora</translation>
    </message>
    <message>
        <source>Print To File ...</source>
        <translation type="vanished">Imprimir Para Ficheiro ...</translation>
    </message>
    <message>
        <source>Print dialog</source>
        <translation type="obsolete">Diálogo de impressão</translation>
    </message>
    <message>
        <source>Paper format</source>
        <translation type="obsolete">Formato do papel</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="obsolete">Tamanho:</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Propriedades</translation>
    </message>
    <message>
        <source>Printer info:</source>
        <translation type="obsolete">Informação da impressora:</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="obsolete">Navegar</translation>
    </message>
    <message>
        <source>Print to file</source>
        <translation type="obsolete">Imprimir para um ficheiro</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="vanished">Selecção de páginas</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="vanished">Imprimir todas</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="obsolete">Páginas de</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">a</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="obsolete">Selecção</translation>
    </message>
    <message>
        <source>Copies</source>
        <translation type="obsolete">Cópias</translation>
    </message>
    <message>
        <source>Number of copies:</source>
        <translation type="obsolete">Número de cópias:</translation>
    </message>
    <message>
        <source>Collate</source>
        <translation type="obsolete">Juntar</translation>
    </message>
    <message>
        <source>Print last page first</source>
        <translation type="obsolete">Imprimir a última página primeiro</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="obsolete">Outro</translation>
    </message>
    <message>
        <source>Print in color if available</source>
        <translation type="obsolete">Imprimir a cores se disponível</translation>
    </message>
    <message>
        <source>Double side printing</source>
        <translation type="obsolete">Imprimir nos dois lados do papel</translation>
    </message>
    <message>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation type="vanished">Não é possível escrever no ficheiro %1.
Por favor escolha um nome diferente.</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation type="vanished">%1 já existe.
Deseja substituir?</translation>
    </message>
    <message>
        <source>File exists</source>
        <translation type="vanished">O ficheiro existe</translation>
    </message>
    <message>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation type="vanished">&lt;qt&gt;Deseja gravar por cima?&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>Print selection</source>
        <translation type="vanished">Selecção de Impressão</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <source>Portrait</source>
        <translation type="obsolete">Retrato</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="obsolete">Paisagem</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fechar</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>PPD Properties</source>
        <translation type="obsolete">Propriedades PPD</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Gravar</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <source>Copies</source>
        <translation type="obsolete">Cópias</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="obsolete">Selecção de páginas</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="obsolete">Imprimir todas</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="obsolete">Páginas de</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">a</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="obsolete">Selecção</translation>
    </message>
    <message>
        <source>Collate</source>
        <translation type="obsolete">Juntar</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Opções</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <source>Printer</source>
        <translation type="obsolete">Impressora</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <source>Check</source>
        <translation type="vanished">Activar</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <source>no error occurred</source>
        <translation type="vanished">sem erros</translation>
    </message>
    <message>
        <source>disabled feature used</source>
        <translation type="vanished">funcionalidade desactivada está a ser utilizada</translation>
    </message>
    <message>
        <source>bad char class syntax</source>
        <translation type="vanished">má sintaxe de classe de caracteres</translation>
    </message>
    <message>
        <source>bad lookahead syntax</source>
        <translation type="vanished">má sintaxe de antecipação</translation>
    </message>
    <message>
        <source>bad repetition syntax</source>
        <translation type="vanished">má sintaxe de repetição</translation>
    </message>
    <message>
        <source>invalid octal value</source>
        <translation type="vanished">valor octal inválido</translation>
    </message>
    <message>
        <source>missing left delim</source>
        <translation type="vanished">delimitador esquerdo em falta</translation>
    </message>
    <message>
        <source>unexpected end</source>
        <translation type="vanished">fim inesperado</translation>
    </message>
    <message>
        <source>met internal limit</source>
        <translation type="vanished">limite interno alcançado</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <source>Error to open database</source>
        <translation type="vanished">Erro ao abrir base de dados</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Não foi possível iniciar a transacção</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Unable to rollback Transaction</source>
        <translation type="vanished">Não foi possível anular a transacção</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <source>Unable to fetch results</source>
        <translation type="vanished">Não foi possível obter os resultados</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Não foi possível executar a expressão</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Erro ao abrir a base de dados</translation>
    </message>
    <message>
        <source>Error closing database</source>
        <translation type="vanished">Erro ao fechar a base de dados</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Não foi possível iniciar a transacção</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Não foi possível finalizar a transacção</translation>
    </message>
    <message>
        <source>Unable to roll back transaction</source>
        <translation type="obsolete">Não foi possível anular a transacção</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <source>Unable to fetch row</source>
        <translation type="vanished">Não foi possível obter a linha</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Não foi possível executar a expressão</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Não foi possível restaurar a expressão</translation>
    </message>
    <message>
        <source>Unable to bind parameters</source>
        <translation type="vanished">Não foi possível fazer a ligação dos parametros</translation>
    </message>
    <message>
        <source>Parameter count mismatch</source>
        <translation type="vanished">Incorrespondência de contagem de parâmetros</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <source>Scroll here</source>
        <translation type="vanished">Deslizar aqui</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="vanished">Borda esquerda</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="vanished">Topo</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="vanished">Borda direita</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="vanished">Fundo</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="vanished">Página para esquerda</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Página para cima</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Página para direita</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="vanished">Página para baixo</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="vanished">Deslizar para esquerda</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="vanished">Deslizar para cima</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="vanished">Deslizar para a direita</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="vanished">Deslizar para baixo</translation>
    </message>
    <message>
        <source>Line up</source>
        <translation type="vanished">Linha acima</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Posição</translation>
    </message>
    <message>
        <source>Line down</source>
        <translation type="vanished">Linha abaixo</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <source>Space</source>
        <translation type="vanished">Space</translation>
    </message>
    <message>
        <source>Esc</source>
        <translation type="vanished">Esc</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Tab</translation>
    </message>
    <message>
        <source>Backtab</source>
        <translation type="vanished">Backtab</translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="vanished">Backspace</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">Return</translation>
    </message>
    <message>
        <source>Enter</source>
        <translation type="vanished">Enter</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="vanished">Insert</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">Pause</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Print</translation>
    </message>
    <message>
        <source>SysReq</source>
        <translation type="vanished">SysReq</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Home</translation>
    </message>
    <message>
        <source>End</source>
        <translation type="vanished">End</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="vanished">Esquerda</translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="vanished">Cima</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="vanished">Direita</translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="vanished">Baixo</translation>
    </message>
    <message>
        <source>PgUp</source>
        <translation type="vanished">PgUp</translation>
    </message>
    <message>
        <source>PgDown</source>
        <translation type="vanished">PgDown</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation type="vanished">CapsLock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation type="vanished">Num Lock</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation type="vanished">ScrollLock</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Menu</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ajuda</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Anterior</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Seguinte</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Parar</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Refrescar</translation>
    </message>
    <message>
        <source>Volume Down</source>
        <translation type="vanished">Volume Cima</translation>
    </message>
    <message>
        <source>Volume Mute</source>
        <translation type="vanished">Volume Mute</translation>
    </message>
    <message>
        <source>Volume Up</source>
        <translation type="vanished">Volume Baixo</translation>
    </message>
    <message>
        <source>Bass Boost</source>
        <translation type="vanished">Bass Boost</translation>
    </message>
    <message>
        <source>Bass Up</source>
        <translation type="vanished">Bass Cima</translation>
    </message>
    <message>
        <source>Bass Down</source>
        <translation type="vanished">Bass Baixo</translation>
    </message>
    <message>
        <source>Treble Up</source>
        <translation type="vanished">Treble Cima</translation>
    </message>
    <message>
        <source>Treble Down</source>
        <translation type="vanished">Treble Baixo</translation>
    </message>
    <message>
        <source>Media Play</source>
        <translation type="vanished">Tocar Média</translation>
    </message>
    <message>
        <source>Media Stop</source>
        <translation type="vanished">Parar Média</translation>
    </message>
    <message>
        <source>Media Previous</source>
        <translation type="vanished">Média Anterior</translation>
    </message>
    <message>
        <source>Media Next</source>
        <translation type="vanished">Média Seguinte</translation>
    </message>
    <message>
        <source>Media Record</source>
        <translation type="vanished">Gravação Média</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Favoritos</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Procurar</translation>
    </message>
    <message>
        <source>Standby</source>
        <translation type="vanished">Hibernação</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="vanished">Abrir Endereço</translation>
    </message>
    <message>
        <source>Launch Mail</source>
        <translation type="vanished">Correio Electrónico</translation>
    </message>
    <message>
        <source>Launch Media</source>
        <translation type="vanished">Média</translation>
    </message>
    <message>
        <source>Launch (0)</source>
        <translation type="vanished">Executar (0)</translation>
    </message>
    <message>
        <source>Launch (1)</source>
        <translation type="vanished">Executar (1)</translation>
    </message>
    <message>
        <source>Launch (2)</source>
        <translation type="vanished">Executar (2)</translation>
    </message>
    <message>
        <source>Launch (3)</source>
        <translation type="vanished">Executar (3)</translation>
    </message>
    <message>
        <source>Launch (4)</source>
        <translation type="vanished">Executar (4)</translation>
    </message>
    <message>
        <source>Launch (5)</source>
        <translation type="vanished">Executar (5)</translation>
    </message>
    <message>
        <source>Launch (6)</source>
        <translation type="vanished">Executar (6)</translation>
    </message>
    <message>
        <source>Launch (7)</source>
        <translation type="vanished">Executar (7)</translation>
    </message>
    <message>
        <source>Launch (8)</source>
        <translation type="vanished">Executar (8)</translation>
    </message>
    <message>
        <source>Launch (9)</source>
        <translation type="vanished">Executar (9)</translation>
    </message>
    <message>
        <source>Launch (A)</source>
        <translation type="vanished">Executar (A)</translation>
    </message>
    <message>
        <source>Launch (B)</source>
        <translation type="vanished">Executar (B)</translation>
    </message>
    <message>
        <source>Launch (C)</source>
        <translation type="vanished">Executar (C)</translation>
    </message>
    <message>
        <source>Launch (D)</source>
        <translation type="vanished">Executar (D)</translation>
    </message>
    <message>
        <source>Launch (E)</source>
        <translation type="vanished">Executar (E)</translation>
    </message>
    <message>
        <source>Launch (F)</source>
        <translation type="vanished">Executar (F)</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">Print Screen</translation>
    </message>
    <message>
        <source>Page Up</source>
        <translation type="vanished">Page Up</translation>
    </message>
    <message>
        <source>Page Down</source>
        <translation type="vanished">Page Down</translation>
    </message>
    <message>
        <source>Caps Lock</source>
        <translation type="vanished">Caps Lock</translation>
    </message>
    <message>
        <source>Num Lock</source>
        <translation type="vanished">Num Lock</translation>
    </message>
    <message>
        <source>Number Lock</source>
        <translation type="vanished">Number Lock</translation>
    </message>
    <message>
        <source>Scroll Lock</source>
        <translation type="vanished">Scroll Lock</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Insert</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Escape</source>
        <translation type="vanished">Escape</translation>
    </message>
    <message>
        <source>System Request</source>
        <translation type="vanished">System Request</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Select</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Sim</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Não</translation>
    </message>
    <message>
        <source>Context1</source>
        <translation type="vanished">Contexto1</translation>
    </message>
    <message>
        <source>Context2</source>
        <translation type="vanished">Contexto2</translation>
    </message>
    <message>
        <source>Context3</source>
        <translation type="vanished">Contexto3</translation>
    </message>
    <message>
        <source>Context4</source>
        <translation type="vanished">Contexto4</translation>
    </message>
    <message>
        <source>Call</source>
        <translation type="vanished">Chamar</translation>
    </message>
    <message>
        <source>Hangup</source>
        <translation type="vanished">Desligar</translation>
    </message>
    <message>
        <source>Flip</source>
        <translation type="vanished">Inverter</translation>
    </message>
    <message>
        <source>Ctrl</source>
        <translation type="vanished">Ctrl</translation>
    </message>
    <message>
        <source>Shift</source>
        <translation type="vanished">Shift</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation type="vanished">Alt</translation>
    </message>
    <message>
        <source>Meta</source>
        <translation type="vanished">Meta</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <source>F%1</source>
        <translation type="vanished">F%1</translation>
    </message>
    <message>
        <source>Home Page</source>
        <translation type="vanished">Página Principal</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <source>Page left</source>
        <translation type="vanished">Página para esquerda</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Página para cima</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Posição</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Página para direita</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="vanished">Página para baixo</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">Socks5 expirado ao ligar ao servidor socks</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="obsolete">Operação de rede expirada</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <source>More</source>
        <translation type="vanished">Mais</translation>
    </message>
    <message>
        <source>Less</source>
        <translation type="vanished">Menos</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <source>Delete</source>
        <translation type="vanished">Apagar</translation>
    </message>
    <message>
        <source>Delete this record?</source>
        <translation type="vanished">Apagar este registo?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Sim</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Não</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Inserir</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Actualizar</translation>
    </message>
    <message>
        <source>Save edits?</source>
        <translation type="vanished">Gravar as alterações?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Confirmar</translation>
    </message>
    <message>
        <source>Cancel your edits?</source>
        <translation type="vanished">Cancelar as alterações?</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <source>Unable to open connection</source>
        <translation type="vanished">Não foi possível estabelecer a ligação</translation>
    </message>
    <message>
        <source>Unable to use database</source>
        <translation type="vanished">Não foi possível utilizar a base de dados</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <source>Scroll Left</source>
        <translation type="vanished">Deslizar para Esquerda</translation>
    </message>
    <message>
        <source>Scroll Right</source>
        <translation type="vanished">Deslizar para Direita</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <source>Socket operation unsupported</source>
        <translation type="obsolete">Operação de &apos;socket&apos; não suportada</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Desfazer</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Refazer</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Cor&amp;tar</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Copiar</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation type="vanished">Copiar &amp;Localização da Ligação</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">Co&amp;lar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Apagar</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Seleccionar Tudo</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <source>Press</source>
        <translation type="vanished">Pressionar</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <source>This platform does not support IPv6</source>
        <translation type="vanished">Esta plataforma não suporta IPv6</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <source>Undo</source>
        <translation type="vanished">Desfazer</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="vanished">Refazer</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <source>&lt;empty&gt;</source>
        <translation type="vanished">&lt;vazio&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <source>Undo</source>
        <translation type="vanished">Desfazer</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="vanished">Refazer</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <source>LRM Left-to-right mark</source>
        <translation type="vanished">LRM Marca esquerda-para-direita</translation>
    </message>
    <message>
        <source>RLM Right-to-left mark</source>
        <translation type="vanished">RLM Marca direita-para-esquerda</translation>
    </message>
    <message>
        <source>ZWJ Zero width joiner</source>
        <translation type="vanished">ZWJ Ligador de comprimento zero</translation>
    </message>
    <message>
        <source>ZWNJ Zero width non-joiner</source>
        <translation type="vanished">ZWNJ Não-ligador de comprimento zero</translation>
    </message>
    <message>
        <source>ZWSP Zero width space</source>
        <translation type="vanished">ZWSP Espaço de comprimento zero</translation>
    </message>
    <message>
        <source>LRE Start of left-to-right embedding</source>
        <translation type="vanished">LRE Início de encaixe esquerda-para-direita</translation>
    </message>
    <message>
        <source>RLE Start of right-to-left embedding</source>
        <translation type="vanished">RLE Início de encaixe direita-para-esquerda</translation>
    </message>
    <message>
        <source>LRO Start of left-to-right override</source>
        <translation type="vanished">LRO Início de sobreposição esquerda-para-direita</translation>
    </message>
    <message>
        <source>RLO Start of right-to-left override</source>
        <translation type="vanished">RLO Início de sobreposição direita-para-esquerda</translation>
    </message>
    <message>
        <source>PDF Pop directional formatting</source>
        <translation type="vanished">PDF Formatação pop direccional</translation>
    </message>
    <message>
        <source>Insert Unicode control character</source>
        <translation type="vanished">Inserir carácter de controlo Unicode</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation type="obsolete">Restaurar</translation>
    </message>
    <message>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation type="obsolete">Parar</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation type="obsolete">Ignorar</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation type="obsolete">Ignorar</translation>
    </message>
    <message>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation type="obsolete">Desconhecido</translation>
    </message>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">Deslizar aqui</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">Borda esquerda</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">Topo</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">Borda direita</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">Fundo</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">Página para esquerda</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">Página para cima</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">Página para direita</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">Página para baixo</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">Deslizar para esquerda</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">Deslizar para cima</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">Deslizar para a direita</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">Deslizar para baixo</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <source>What&apos;s This?</source>
        <translation type="vanished">O Que é Isto?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <source>*</source>
        <translation type="vanished">*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Sair</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ajuda</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation type="obsolete">&lt; &amp;Recuar</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation type="obsolete">&amp;Terminar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Ajuda</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="obsolete">&amp;Avançar &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">&amp;Restaurar</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="vanished">&amp;Mover</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Tamanho</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">Mi&amp;nimizar</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="vanished">Ma&amp;ximizar</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">&amp;Fechar</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation type="vanished">Permanecer no &amp;Topo</translation>
    </message>
    <message>
        <source>Sh&amp;ade</source>
        <translation type="vanished">Sombr&amp;a</translation>
    </message>
    <message>
        <source>%1 - [%2]</source>
        <translation type="vanished">%1 - [%2]</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Minimizar</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation type="vanished">Restaurar Baixo</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Fechar</translation>
    </message>
    <message>
        <source>&amp;Unshade</source>
        <translation type="vanished">&amp;Sair Sombra</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <source>no error occurred</source>
        <translation type="vanished">não ocorreu nenhum erro</translation>
    </message>
    <message>
        <source>error triggered by consumer</source>
        <translation type="vanished">erro disparado pelo consumidor</translation>
    </message>
    <message>
        <source>unexpected end of file</source>
        <translation type="vanished">fim de ficheiro inesperado</translation>
    </message>
    <message>
        <source>more than one document type definition</source>
        <translation type="vanished">mais de uma definição de tipo de documento</translation>
    </message>
    <message>
        <source>error occurred while parsing element</source>
        <translation type="vanished">erro ao analisar elemento</translation>
    </message>
    <message>
        <source>tag mismatch</source>
        <translation type="vanished">má combinação de etiqueta</translation>
    </message>
    <message>
        <source>error occurred while parsing content</source>
        <translation type="vanished">erro ao analisar o conteúdo</translation>
    </message>
    <message>
        <source>unexpected character</source>
        <translation type="vanished">carácter inesperado</translation>
    </message>
    <message>
        <source>invalid name for processing instruction</source>
        <translation type="vanished">nome inválido de instrução de processamento</translation>
    </message>
    <message>
        <source>version expected while reading the XML declaration</source>
        <translation type="vanished">versão esperada ao ler a declaração XML</translation>
    </message>
    <message>
        <source>wrong value for standalone declaration</source>
        <translation type="vanished">valor errado para declaração única</translation>
    </message>
    <message>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation type="vanished">declaração de codificação ou declaração única esperada ao ler a declaração XML</translation>
    </message>
    <message>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation type="vanished">declaração única esperada ao ler a declaração XML</translation>
    </message>
    <message>
        <source>error occurred while parsing document type definition</source>
        <translation type="vanished">erro ao analisar a definição de tipo de documento</translation>
    </message>
    <message>
        <source>letter is expected</source>
        <translation type="vanished">uma letra é esperada</translation>
    </message>
    <message>
        <source>error occurred while parsing comment</source>
        <translation type="vanished">erro ao analisar comentário</translation>
    </message>
    <message>
        <source>error occurred while parsing reference</source>
        <translation type="vanished">erro ao analisar referência</translation>
    </message>
    <message>
        <source>internal general entity reference not allowed in DTD</source>
        <translation type="vanished">referência de entidade geral interna não permitida na DTD</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation type="vanished">referência de entidade geral analisada externa não permitida no valor do atributo</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation type="vanished">referência de entidade geral analisada externa não permitida na DTD</translation>
    </message>
    <message>
        <source>unparsed entity reference in wrong context</source>
        <translation type="vanished">referência de entidade não analisada em contexto errado</translation>
    </message>
    <message>
        <source>recursive entities</source>
        <translation type="vanished">entidades recursivas</translation>
    </message>
    <message>
        <source>error in the text declaration of an external entity</source>
        <translation type="vanished">erro na declaração de uma entidade externa</translation>
    </message>
</context>
<context>
    <name>QtFrontend</name>
    <message>
        <location filename="../src/frontends/qtfrontend/qtfrontend.cpp" line="+137"/>
        <source>Create directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>You do not have the necessary permissions to run qStopMotion.
You need permission to create the .qstopmotion directory in your home directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Check Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>You do not have the necessary permissions to run qStopMotion.
You need permission to read, write and execute on the .qstopmotion directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+600"/>
        <source>&amp;Yes</source>
        <translation type="unfinished">&amp;Sim</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;No</source>
        <translation type="unfinished">&amp;Não</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Something caused qStopmotion to exit abnormally
last time it was run. Do you want to recover?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>qStopMotion - Recovered Project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordingTab</name>
    <message>
        <location filename="../src/frontends/qtfrontend/tooltabs/recordingtab.cpp" line="+298"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video Source:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Recording mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Single frame capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time-lapse capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Toggle camera on/off (C)&lt;/h4&gt; &lt;p&gt;Click this button to toggle the camera on and off&lt;/p&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Number of images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;h4&gt;Number of images&lt;/h4&gt; &lt;p&gt;By changing the value in this slidebar you can specify how many images backwards in the animation which should be mixed on top of the camera or if you are in playback mode: how many images to play. &lt;/p&gt; &lt;p&gt;By mixing the previous image(s) onto the camera you can more easily see how the next shot will be in relation to the other, therby making a smoother stop motion animation!&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Time-lapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>seconds before picture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Not Supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+341"/>
        <source>Seconds between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Minutes between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Hours between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Days between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <location line="+6"/>
        <location line="+6"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-12"/>
        <source>No active project. Please create a new project or open an existing project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No scene and no take selected. Please select a scene and a take on the project tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No take selected. Please select a take on the project tab.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scene</name>
    <message>
        <location filename="../src/domain/animation/scene.cpp" line="+694"/>
        <location line="+66"/>
        <location line="+4"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-69"/>
        <location line="+70"/>
        <source>Can&apos;t remove sound file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Can&apos;t copy sound file!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/startdialog.cpp" line="+62"/>
        <source>&lt;h2&gt;Create a new Project&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Start a new Project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>&lt;h2&gt;Open last Project&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Project path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>&lt;h2&gt;Open project file&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open and continue a existing project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Fechar</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/frontends/qtfrontend/timeline/timeline.cpp" line="+707"/>
        <source>Load images to time line</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/frontends/qtfrontend/toolbar.cpp" line="+175"/>
        <source>&lt;h4&gt;Degree of overlay&lt;/h4&gt;&lt;p&gt;Change the degree of superposition of the previous exposure to live video from camera&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&lt;h4&gt;Play animation (K, P)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;h4&gt;First frame of the take (J, Left)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>&lt;h4&gt;Previous frame (J, Left)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Next frame (L, Right)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Last frame of the take (L, Right)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&lt;h4&gt;Capture Frame (Space)&lt;/h4&gt; &lt;p&gt;Click on this button to &lt;em&gt;capture&lt;/em&gt; a frame from the camera an put it in the animation&lt;/p&gt; &lt;p&gt; This can also be done by pressing the &lt;b&gt;Space key&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Running animation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransformWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/transformwidget.cpp" line="+90"/>
        <source>Below you can set which image transformation should be used for importing images to a new project. If you select clip a part of the image set also the adjustment for cliping.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Below you can set which image transformation should be used for importing images to the currently active project. If you select clip a part of the image set also the adjustment for cliping.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Transformation settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scale the whole image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clip a part of the image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom the image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Zoom settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom value (%):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureAdd</name>
    <message>
        <location filename="../src/domain/undo/undoexposureadd.cpp" line="+44"/>
        <source>Add exposure (%1,%2,%3) &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureInsert</name>
    <message>
        <location filename="../src/domain/undo/undoexposureinsert.cpp" line="+44"/>
        <source>Insert exposure (%1,%2,%3) &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureMove</name>
    <message>
        <location filename="../src/domain/undo/undoexposuremove.cpp" line="+45"/>
        <source>Move exposure (%1,%2,%3)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureRemove</name>
    <message>
        <location filename="../src/domain/undo/undoexposureremove.cpp" line="+42"/>
        <source>Remove exposure (%1,%2,%3)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureSelect</name>
    <message>
        <location filename="../src/domain/undo/undoexposureselect.cpp" line="+40"/>
        <source>Select exposure (%1,%2,%3)--&gt;(%4,%5,%6)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoProjectNew</name>
    <message>
        <location filename="../src/domain/undo/undoprojectnew.cpp" line="+37"/>
        <source>New project &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoProjectOpen</name>
    <message>
        <location filename="../src/domain/undo/undoprojectopen.cpp" line="+35"/>
        <source>Open project &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneAdd</name>
    <message>
        <location filename="../src/domain/undo/undosceneadd.cpp" line="+36"/>
        <source>Add scene (%1) &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneInsert</name>
    <message>
        <location filename="../src/domain/undo/undosceneinsert.cpp" line="+37"/>
        <source>Insert scene (%1) &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneMove</name>
    <message>
        <location filename="../src/domain/undo/undoscenemove.cpp" line="+37"/>
        <source>Move scene (%1,%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneRemove</name>
    <message>
        <location filename="../src/domain/undo/undosceneremove.cpp" line="+36"/>
        <source>Remove scene (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneSelect</name>
    <message>
        <location filename="../src/domain/undo/undosceneselect.cpp" line="+34"/>
        <source>Select scene (%1)--&gt;(%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeAdd</name>
    <message>
        <location filename="../src/domain/undo/undotakeadd.cpp" line="+39"/>
        <source>Add take (%1,%2) &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeInsert</name>
    <message>
        <location filename="../src/domain/undo/undotakeinsert.cpp" line="+40"/>
        <source>Insert take (%1,%2) &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeMove</name>
    <message>
        <location filename="../src/domain/undo/undotakemove.cpp" line="+39"/>
        <source>Move take (%1,%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeRemove</name>
    <message>
        <location filename="../src/domain/undo/undotakeremove.cpp" line="+38"/>
        <source>Remove take (%1,%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeSelect</name>
    <message>
        <location filename="../src/domain/undo/undotakeselect.cpp" line="+37"/>
        <source>Select take (%1,%2)--&gt;(%3,%4)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
