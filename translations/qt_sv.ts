<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/aboutdialog.cpp" line="+53"/>
        <source>This is the qStopMotion application for creating stop motion animations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+6"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2"/>
        <source>qStopMotion is a fork of stopmotion for linux.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+4"/>
        <source>Main developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A&amp;uthors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Danish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Czech</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&amp;Thanks To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This program is distributed under the terms of the GPL v2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Licence Agreement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Qt runtime version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operating system name and version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Build time: %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;System Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnimationProject</name>
    <message>
        <location filename="../src/domain/animation/animationproject.cpp" line="+577"/>
        <source>Saving scenes to disk ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+972"/>
        <location line="+7"/>
        <source>Add Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Cannot open the selected audio file for reading.
Check that you have the right permissions set.
The animation will be run without sound if you
choose to play.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The selected audio file is not valid within the
given audio format. The animation will be run
without sound if you choose to play.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraControllerDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/cameracontrollerdialog.cpp" line="+270"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>qStopMotion Camera Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Video Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Brightness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contrast:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Saturation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Hue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gamma:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Sharpness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sharpness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Backlight Compensation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backlight Compensation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic White Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>White Balance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Color Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Color Enable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Camera Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic Exposure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exposure:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Focus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Pan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Tilt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tilt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Iris:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatic Roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Roll:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Reset to Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Stäng</translation>
    </message>
    <message>
        <location line="+806"/>
        <source>Restore Camera Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1404"/>
        <source>Reset Camera Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Abort Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ControllerWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/controllerwidget.cpp" line="+104"/>
        <source>Below you can set which features of the camera should be used in the camera control dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Image quality controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Brightness control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Contrast control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Saturation control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hue control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gamma control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sharpness control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backlight compensation control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>White balance control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gain control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Color enable control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Camera controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Exposure control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Focus control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pan control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tilt control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Iris control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Roll control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DescriptionDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/descriptiondialog.cpp" line="+35"/>
        <source>&amp;Project Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Scene Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Take Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Avbryt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Project Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Scene Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Take Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+11"/>
        <location line="+11"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-22"/>
        <source>The character &apos;|&apos; is not allowed in the project description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The character &apos;|&apos; is not allowed in the scene description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The character &apos;|&apos; is not allowed in the take description.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DomainFacade</name>
    <message>
        <location filename="../src/domain/domainfacade.cpp" line="+152"/>
        <location line="+411"/>
        <location line="+775"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1185"/>
        <source>Can&apos;t open history file to write entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+411"/>
        <source>Can&apos;t open history file to recover project!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+775"/>
        <source>Can&apos;t copy image to temp directory!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/exportwidget.cpp" line="+97"/>
        <source>Below you can set which program should be used for encoding a new project to a video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Below you can set which program should be used for encoding the currently active project to a video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Encoder settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Encoder Application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ffmpeg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>libav</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>AVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MP4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Frame Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>QVGA (320x240)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>VGA (640x480)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SVGA (800x600)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PAL D (704x576)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>HD Ready (1280x720)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full HD (1900x1080)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Frames per Second:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;FPS chooser&lt;/h4&gt; &lt;p&gt;By changing the value in this chooser you set which speed the animation in the &lt;b&gt;FrameView&lt;/b&gt; should run at.&lt;/p&gt; &lt;p&gt;To start an animation press the &lt;b&gt;Run Animation&lt;/b&gt; button.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Splitting up the movie on several files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All scenes and takes will be united in one movie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Movie will be splitted up by scenes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Movie will be splitted up by takes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Output file settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Do you want to be asked for an output file everytime you choose to export?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No</source>
        <translation type="unfinished">Nej</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set default output file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Browse</source>
        <translation type="unfinished">Bläddra</translation>
    </message>
    <message>
        <location line="+342"/>
        <source>Choose output file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Exposure</name>
    <message>
        <location filename="../src/domain/animation/exposure.cpp" line="+199"/>
        <location line="+4"/>
        <location line="+41"/>
        <location line="+57"/>
        <location line="+33"/>
        <location line="+4"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-138"/>
        <location line="+45"/>
        <source>Can&apos;t copy the image to the temporary directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Can&apos;t remove the image from the project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Can&apos;t save the image in the new file format to the temporary directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Can&apos;t copy the image to the project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t remove the image in the temporary directory!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExternalCommandDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/externalcommanddialog.cpp" line="+50"/>
        <source>Input to program:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Close</source>
        <translation type="unfinished">Stäng</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Output from external command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <location line="+4"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Successful!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/generaldialog.cpp" line="+64"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Apply</source>
        <translation type="unfinished">Verkställ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close</source>
        <translation type="unfinished">Stäng</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>qStopMotion Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>New Project Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Image Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Image Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Video Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Camera Controller</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/generalwidget.cpp" line="+99"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Capture Button Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Insert new frame bevor selected frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Insert new frame after selected frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Append new frame at the end of the take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Grid Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Vertical Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Horizontal Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Grid Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Signal Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take Picture Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Image editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Select application for edit exposures:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pease restart qStopMotion to activate the new style!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+181"/>
        <source>Choose image editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GrabberWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/grabberwidget.cpp" line="+75"/>
        <source>Below you can select which image grabber should be used for grabbing images from the camera. If available the controller can be used to control focus, zoom and other functionality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Grabber Functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video 4 Linux 2 Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Camera Controller (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Microsoft Media Foundation Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Gphoto2 Source (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+135"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pease restart qStopMotion to activate the new grabber stettings!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/helpbrowser.cpp" line="+39"/>
        <source>qStopMotion Help Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Stäng</translation>
    </message>
</context>
<context>
    <name>ImageGrabberFacade</name>
    <message>
        <location filename="../src/technical/grabber/imagegrabberfacade.cpp" line="+162"/>
        <source>Check image sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No usable video image source found. Please
connect on video device on the computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Check image grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Image grabber initialization failed. This may happen 
if you try to grab from an invalid device. Please
select another device on the recording tool tab.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/importwidget.cpp" line="+84"/>
        <source>Below you can set which image sources should be used for importing images to a new project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Below you can set which image sources should be used for importing images to the currently active project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Image import settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Image Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>JPEG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TIFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>BMP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Image Quality:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Image Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Default Grabber Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>QVGA (320x240)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>VGA (640x480)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SVGA (800x600)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PAL D (704x576)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>HD Ready (1280x720)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full HD (1900x1080)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Live view settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Frames per second:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <source>Services</source>
        <translation type="vanished">Tjänster</translation>
    </message>
    <message>
        <source>Hide %1</source>
        <translation type="vanished">Göm %1</translation>
    </message>
    <message>
        <source>Hide Others</source>
        <translation type="vanished">Göm övriga</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation type="vanished">Visa alla</translation>
    </message>
    <message>
        <source>Preferences...</source>
        <translation type="vanished">Inställningar…</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="vanished">Avsluta %1</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="vanished">Om %1</translation>
    </message>
</context>
<context>
    <name>MainWindowGUI</name>
    <message>
        <location filename="../src/frontends/qtfrontend/mainwindowgui.cpp" line="+188"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Öppna</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Save</source>
        <translation type="unfinished">&amp;Spara</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save &amp;As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cinelerra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Properties</source>
        <translation type="unfinished">Egenskaper</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Undo</source>
        <translation type="unfinished">&amp;Ångra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Re&amp;do</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Undo stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Camera Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>What&apos;s &amp;This</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+12"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Hjälp</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+212"/>
        <source>Online Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-211"/>
        <location line="+219"/>
        <source>Support Us</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-218"/>
        <location line="+227"/>
        <source>About Qt</source>
        <translation type="unfinished">Om Qt</translation>
    </message>
    <message>
        <location line="-226"/>
        <location line="+236"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-233"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open &amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Project ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scene ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Take ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exposure ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>&lt;h4&gt;New&lt;/h4&gt; &lt;p&gt;Creates a &lt;em&gt;new&lt;/em&gt; project.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Open&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Opens&lt;/em&gt; a qStopMotion project file.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Open project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Save&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Saves&lt;/em&gt; the current animation as a qStopMotion project file. &lt;BR&gt;If this project has been saved before it will automatically be saved to the previously selected file.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Save As&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Saves&lt;/em&gt; the current animation as a qStopMotion project file.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save project As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Video&lt;/h4&gt; &lt;p&gt;Exports the current project as &lt;em&gt;video&lt;/em&gt;.&lt;/p&gt;You will be given a wizard to guide you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Cinelerra&lt;/h4&gt; &lt;p&gt;Exports the current animation as a &lt;em&gt;cinelerra-cv&lt;/em&gt; project.&lt;/p&gt;You will be given a wizard to guide you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Properties of the project&lt;/h4&gt; &lt;p&gt;This will opens a window where you can &lt;em&gt;change&lt;/em&gt; properties of the animation project.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Properties of the animation project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Quit&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Quits&lt;/em&gt; the program.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;h4&gt;Undo&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Undoes&lt;/em&gt; your last operation. You can press undo several time to undo earlier operations.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Undo</source>
        <translation type="unfinished">Ångra</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Redo&lt;/h4&gt; &lt;p&gt;&lt;em&gt;Redoes&lt;/em&gt; your last operation. You can press redo several times to redo several operations.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Redo</source>
        <translation type="unfinished">Gör om</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>&lt;h4&gt;Preferences of the application&lt;/h4&gt; &lt;p&gt;This will opens a window where you can &lt;em&gt;change&lt;/em&gt; the preferences of the application.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Preferences of qStopMotion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;What&apos;s This&lt;/h4&gt; &lt;p&gt;This will give you a WhatsThis mouse cursor which can be used to bring up helpful information like this.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>What&apos;s This</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Help&lt;/h4&gt; &lt;p&gt;This button will bring up a dialog with the qStopMotion manual&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;h4&gt;Online Help&lt;/h4&gt; &lt;p&gt;This button will bring up the browser with the qStopMotion manual in different languages&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Support Us&lt;/h4&gt; &lt;p&gt;This button will bring up the browser with the qStopMotion support us page&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;About Qt&lt;/h4&gt; &lt;p&gt;This will display a small information box where you can read general information about the Qt library.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;h4&gt;About&lt;/h4&gt; &lt;p&gt;This will display a small information box where you can read general information as well as the names of the developers behind this excellent piece of software.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&lt;h4&gt;Project ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently active project&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Scene ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently selected scene&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Take ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently selected take&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;h4&gt;Exposure ID&lt;/h4&gt;&lt;p&gt;This area displays the id of the currently selected exposure&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;h4&gt;FrameView&lt;/h4&gt;&lt;p&gt; In this area you can see the selected frame. You can also play animations in this window by pressing the &lt;b&gt;Play&lt;/b&gt; button.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;h4&gt;TimeLine&lt;/h4&gt; &lt;p&gt;In this area you can see the frames and scenes in the animations and build the animation by moving the them around.&lt;/p&gt;&lt;p&gt;You can switch to the next and the previous frame using the &lt;b&gt;arrow buttons&lt;/b&gt; or &lt;b&gt;x&lt;/b&gt; and &lt;b&gt;z&lt;/b&gt;&lt;/p&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+385"/>
        <source>Select image grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-384"/>
        <source>You have to define an image grabber to use.
This can be set in the preferences menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Ready to rumble ;-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+118"/>
        <source>No style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Cancel</source>
        <translation type="unfinished">Avbryt</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Connecting camera...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>You have to define an image grabber to use.
This can be set on the recording tool tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Existing Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There are some images in the open project. Do you want to convert the images to the new file format or quality?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+137"/>
        <source>The Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scene 001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Take 01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>qStopMotion - New Animation Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Choose project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+85"/>
        <source>Project (*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Save As</source>
        <translation type="unfinished">Spara som</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The character &apos;|&apos; is not allowed in the project file name and will be removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+30"/>
        <location line="+40"/>
        <location line="+36"/>
        <location line="+26"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-132"/>
        <source>The project directory must not contain more than one project file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>No encoder selected for the video export.
This can be set in the properties dialog of the project.
Export to video will not be possible until you
have set an encoder to use!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>AVI Videos (*.avi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>MP4 Videos (*.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No video format selected for the video export.
This can be set in the properties dialog of the project.
Export to video will not be possible until you
have set an video format to use!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Export to video file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>No default output file name defined.
Check your settings in the properties dialo of the project!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The selected encoder is not installed on your computer.
Install the encoder or select another one!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Exporting ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Export to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+173"/>
        <source>qStopMotion - Undo stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+184"/>
        <source>English</source>
        <comment>This should be translated to the name of the language you are translating to, in that language. Example: English = Deutsch (Deutsch is &quot;German&quot; in German)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+808"/>
        <source>Unsaved changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There are unsaved changes. Do you want to save?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PPDOptionsModel</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Namn</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Värde</translation>
    </message>
</context>
<context>
    <name>PreferencesTool</name>
    <message>
        <location filename="../src/technical/preferencestool.cpp" line="+117"/>
        <location line="+4"/>
        <source>DOM Parser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Couldn&apos;t open XML file:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Parse error at line %1, column %2:
%3
%4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+432"/>
        <location line="+7"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Can&apos;t remove preferences backup file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Can&apos;t copy preferences file to backup file!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/projectdialog.cpp" line="+62"/>
        <source>Properties</source>
        <translation type="unfinished">Egenskaper</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Apply</source>
        <translation type="unfinished">Verkställ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close</source>
        <translation type="unfinished">Stäng</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Animation Project Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Image Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Image Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Video Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectSerializer</name>
    <message>
        <location filename="../src/domain/animation/projectserializer.cpp" line="+75"/>
        <location line="+6"/>
        <source>DOM Parser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Couldn&apos;t open XML file:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Parse error at line %1, column %2:
%3
%4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <location line="+7"/>
        <location line="+4"/>
        <location line="+84"/>
        <location line="+4"/>
        <location line="+6"/>
        <location line="+5"/>
        <location line="+6"/>
        <location line="+4"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-119"/>
        <source>Can&apos;t remove old backup of project file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Can&apos;t copy the project file to backup!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t remove the old project file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Can&apos;t create project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t change permissions of the project directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can&apos;t create image directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Can&apos;t change permissions of the image directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can&apos;t create sound directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Can&apos;t change permissions of the sound directory!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectTab</name>
    <message>
        <location filename="../src/frontends/qtfrontend/tooltabs/projecttab.cpp" line="+314"/>
        <source>Project Tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scenes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Insert new Scene (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;scene&lt;/em&gt; and &lt;em&gt;insert&lt;/em&gt; it bevor the selected scene.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Append new Scene (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;scene&lt;/em&gt; and &lt;em&gt;append&lt;/em&gt; it at the end of the animation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Remove Scene (SHIFT+Delete)&lt;/h4&gt; &lt;p&gt;Click this button to &lt;em&gt;remove&lt;/em&gt; the selected &lt;em&gt;scene&lt;/em&gt; from the animation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Takes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Insert new Take (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;take&lt;/em&gt; and &lt;em&gt;insert&lt;/em&gt; it bevor the selected take.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Append new Take (CTRL+E)&lt;/h4&gt; &lt;p&gt;Click this button to create a new &lt;em&gt;take&lt;/em&gt; and &lt;em&gt;append&lt;/em&gt; it to the end of the scene.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Remove Take (SHIFT+Delete)&lt;/h4&gt; &lt;p&gt;Click this button to &lt;em&gt;remove&lt;/em&gt; the selected &lt;em&gt;take&lt;/em&gt; from the animation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Insert Frames (CTRL+F)&lt;/h4&gt; &lt;p&gt;Click on this button to &lt;em&gt;insert frames&lt;/em&gt; bevor the selected frame.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Append Frames (CTRL+F)&lt;/h4&gt; &lt;p&gt;Click on this button to &lt;em&gt;append frames&lt;/em&gt; at the end of the take.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;h4&gt;Remove Frame (Delete)&lt;/h4&gt; &lt;p&gt;Click this button to &lt;em&gt;remove&lt;/em&gt; the selected &lt;em&gt;frame&lt;/em&gt; from the take.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Launch Photo Editor&lt;/h4&gt; &lt;p&gt;Click this button to open the active frame in the photo editor&lt;/p&gt; &lt;p&gt;Note that you can also drag images from the frame bar and drop them on the photo editor&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+759"/>
        <location line="+35"/>
        <source>Scene 000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-34"/>
        <location line="+35"/>
        <location line="+95"/>
        <location line="+36"/>
        <source>Take 00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-43"/>
        <source>Insert Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+36"/>
        <location line="+89"/>
        <location line="+38"/>
        <source>No scene selected. Please select a scene in the project tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-127"/>
        <source>Add Take</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+5"/>
        <source>Insert Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+38"/>
        <source>No take selected. Please select a take in the project tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <location line="+5"/>
        <source>Add Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+7"/>
        <location line="+209"/>
        <location line="+42"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-257"/>
        <source>There is no active frame to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The active frame is corrupt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Choose frames to add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>You do not have any photo editor installed on your system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Failed to start Photo editor!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/projectwidget.cpp" line="+92"/>
        <source>Image grabber settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Video 4 Linux 2 (USB WebCam)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>gphoto (USB Compact Camera)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Media Foundation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Single frame capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time-lapse capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Mix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number of images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;h4&gt;Number of images&lt;/h4&gt; &lt;p&gt;By changing the value in this slidebar you can specify how many images backwards in the animation which should be mixed on top of the camera.&lt;/p&gt; &lt;p&gt;By mixing the previous image(s) onto the camera you can more easily see how the next shot will be in relation to the other, therby making a smoother stop motion animation!&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Time-lapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>seconds before picture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+266"/>
        <source>Seconds between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Minutes between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Hours between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Days between pictures</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <source>%1, %2 not defined</source>
        <translation type="vanished">%1, %2 är inte definierad</translation>
    </message>
    <message>
        <source>Ambiguous %1 not handled</source>
        <translation type="vanished">Tvetydigt %1 hanteras inte</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <source>True</source>
        <translation type="vanished">Sant</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="vanished">Falskt</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Infoga</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Ta bort</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <source>Copy or Move a File</source>
        <translation type="vanished">Kopiera eller ta bort en fil</translation>
    </message>
    <message>
        <source>Read: %1</source>
        <translation type="vanished">Läs: %1</translation>
    </message>
    <message>
        <source>Write: %1</source>
        <translation type="vanished">Skriv: %1</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Avbryt</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Alla filer (*)</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Namn</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Storlek</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Typ</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Datum</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation type="vanished">Attribut</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
    <message>
        <source>Look &amp;in:</source>
        <translation type="vanished">Leta &amp;i:</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation type="vanished">Fil&amp;namn:</translation>
    </message>
    <message>
        <source>File &amp;type:</source>
        <translation type="vanished">Fil&amp;typ:</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Tillbaka</translation>
    </message>
    <message>
        <source>One directory up</source>
        <translation type="vanished">En katalog uppåt</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation type="vanished">Skapa ny mapp</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Listvy</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation type="vanished">Detaljvy</translation>
    </message>
    <message>
        <source>Preview File Info</source>
        <translation type="vanished">Förhandsgranska filinformation</translation>
    </message>
    <message>
        <source>Preview File Contents</source>
        <translation type="vanished">Förhandsgranska filinnehåll</translation>
    </message>
    <message>
        <source>Read-write</source>
        <translation type="vanished">Läs-skriv</translation>
    </message>
    <message>
        <source>Read-only</source>
        <translation type="vanished">Skrivskyddad</translation>
    </message>
    <message>
        <source>Write-only</source>
        <translation type="vanished">Lässkyddad</translation>
    </message>
    <message>
        <source>Inaccessible</source>
        <translation type="vanished">Otillgänglig</translation>
    </message>
    <message>
        <source>Symlink to File</source>
        <translation type="vanished">Symbolisk länk till fil</translation>
    </message>
    <message>
        <source>Symlink to Directory</source>
        <translation type="vanished">Symbolisk länk till katalog</translation>
    </message>
    <message>
        <source>Symlink to Special</source>
        <translation type="vanished">Symbolisk länk till special</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Fil</translation>
    </message>
    <message>
        <source>Dir</source>
        <translation type="vanished">Katalog</translation>
    </message>
    <message>
        <source>Special</source>
        <translation type="vanished">Special</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="vanished">Spara som</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Öppna</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Spara</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Byt namn</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Ta bort</translation>
    </message>
    <message>
        <source>R&amp;eload</source>
        <translation type="vanished">Uppdat&amp;era</translation>
    </message>
    <message>
        <source>Sort by &amp;Name</source>
        <translation type="vanished">Sortera efter &amp;namn</translation>
    </message>
    <message>
        <source>Sort by &amp;Size</source>
        <translation type="vanished">Sortera efter &amp;storlek</translation>
    </message>
    <message>
        <source>Sort by &amp;Date</source>
        <translation type="vanished">Sortera efter &amp;datum</translation>
    </message>
    <message>
        <source>&amp;Unsorted</source>
        <translation type="vanished">&amp;Osorterad</translation>
    </message>
    <message>
        <source>Sort</source>
        <translation type="vanished">Sortera</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation type="vanished">Visa &amp;dolda filer</translation>
    </message>
    <message>
        <source>the file</source>
        <translation type="vanished">filen</translation>
    </message>
    <message>
        <source>the directory</source>
        <translation type="vanished">katalogen</translation>
    </message>
    <message>
        <source>the symlink</source>
        <translation type="vanished">symboliska länken</translation>
    </message>
    <message>
        <source>Delete %1</source>
        <translation type="vanished">Ta bort %1</translation>
    </message>
    <message>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation type="vanished">&lt;qt&gt;Är du säker på att du vill ta bort %1 &quot;%2&quot;?&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Ja</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Nej</translation>
    </message>
    <message>
        <source>New Folder 1</source>
        <translation type="vanished">Ny mapp 1</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">Ny mapp</translation>
    </message>
    <message>
        <source>New Folder %1</source>
        <translation type="vanished">Ny mapp %1</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation type="vanished">Hitta katalog</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Kataloger</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation type="vanished">Katalog:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fel</translation>
    </message>
    <message>
        <source>%1
File not found.
Check path and filename.</source>
        <translation type="vanished">%1
Filen hittades inte. 
Kontrollera sökväg och filnamn.</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Alla filer (*.*)</translation>
    </message>
    <message>
        <source>Open </source>
        <translation type="vanished">Öppna</translation>
    </message>
    <message>
        <source>Select a Directory</source>
        <translation type="vanished">Välj en katalog</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <source>Could not read directory
%1</source>
        <translation type="vanished">Kunde inte läsa katalogen
%1</translation>
    </message>
    <message>
        <source>Could not create directory
%1</source>
        <translation type="vanished">Kunde inte skapa katalogen 
%1</translation>
    </message>
    <message>
        <source>Could not remove file or directory
%1</source>
        <translation type="vanished">Kunde inte ta bort filen eller katalogen 
%1</translation>
    </message>
    <message>
        <source>Could not rename
%1
to
%2</source>
        <translation type="vanished">Kunde inte byta namn på 
%1 
till 
%2</translation>
    </message>
    <message>
        <source>Could not open
%1</source>
        <translation type="vanished">Kunde inte öppna 
%1</translation>
    </message>
    <message>
        <source>Could not write
%1</source>
        <translation type="vanished">Kunde inte skriva till 
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <source>Line up</source>
        <translation type="vanished">Rada upp</translation>
    </message>
    <message>
        <source>Customize...</source>
        <translation type="vanished">Anpassa...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <source>Operation stopped by the user</source>
        <translation type="vanished">Åtgärden stoppades av användaren</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Avbryt</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Verkställ</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjälp</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="vanished">Standardvärden</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Avbryt</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Ångra</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Gör om</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Klipp u&amp;t</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopiera</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">Klistra &amp;in</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Töm</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Markera alla</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <source>System</source>
        <translation type="vanished">System</translation>
    </message>
    <message>
        <source>Restore up</source>
        <translation type="vanished">Återställ uppåt</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Minimera</translation>
    </message>
    <message>
        <source>Restore down</source>
        <translation type="vanished">Återställ nedåt</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Maximera</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Stäng</translation>
    </message>
    <message>
        <source>Contains commands to manipulate the window</source>
        <translation type="vanished">Innehåller kommandon för att manipulera fönstret</translation>
    </message>
    <message>
        <source>Puts a minimized back to normal</source>
        <translation type="vanished">Återställer ett minimerat till normalt</translation>
    </message>
    <message>
        <source>Moves the window out of the way</source>
        <translation type="vanished">Flyttar fönstret ur vägen</translation>
    </message>
    <message>
        <source>Puts a maximized window back to normal</source>
        <translation type="vanished">Återställer ett maximerat fönster tillbaka till normalt</translation>
    </message>
    <message>
        <source>Makes the window full screen</source>
        <translation type="vanished">Gör fönstret till helskärm</translation>
    </message>
    <message>
        <source>Closes the window</source>
        <translation type="vanished">Stänger fönstret</translation>
    </message>
    <message>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation type="vanished">Visar namnet på fönstret och innehåller kontroller för att manipulera det</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <source>More...</source>
        <translation type="vanished">Mer...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <source>The protocol `%1&apos; is not supported</source>
        <translation type="vanished">Protokollet \&quot;%\&quot; stöds inte</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att lista kataloger</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att skapa nya kataloger</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att ta bort filer eller kataloger</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att byta namn på filer eller kataloger</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att hämta filer</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att lämna filer</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation type="vanished">Protokollet \&quot;%1\&quot; har inte stöd för att kopiera eller flytta filer eller kataloger</translation>
    </message>
    <message>
        <source>(unknown)</source>
        <translation type="vanished">(okänt)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Avbryt</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation type="vanished">&lt; Till&amp;baka</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="vanished">&amp;Nästa &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation type="vanished">&amp;Färdig</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Hjälp</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Värden hittades inte</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Anslutningen nekades</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="obsolete">Tidsgränsen för anslutning överstegs</translation>
    </message>
    <message>
        <source>Socket operation timed out</source>
        <translation type="vanished">Tidsgräns för uttagsåtgärd överstegs</translation>
    </message>
    <message>
        <source>Socket is not connected</source>
        <translation type="vanished">Uttaget är inte anslutet</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="obsolete">Nätverket är inte nåbart</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <source>&amp;Step up</source>
        <translation type="vanished">&amp;Stega uppåt</translation>
    </message>
    <message>
        <source>Step &amp;down</source>
        <translation type="vanished">Stega &amp;nedåt</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Activate</source>
        <translation type="vanished">Aktivera</translation>
    </message>
    <message>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="vanished">Binären \&quot;%1\&quot; kräver Qt %2, hittade Qt %3.</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation type="vanished">Inkompatibelt Qt-biblioteksfel</translation>
    </message>
    <message>
        <source>Activates the program&apos;s main window</source>
        <translation type="vanished">Aktiverar programmets huvudfönster</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <source>Select ActiveX Control</source>
        <translation type="vanished">Välj ActiveX Control</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Avbryt</translation>
    </message>
    <message>
        <source>COM &amp;Object:</source>
        <translation type="vanished">COM-&amp;objekt:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <source>Uncheck</source>
        <translation type="vanished">Avkryssa</translation>
    </message>
    <message>
        <source>Check</source>
        <translation type="vanished">Kryssa</translation>
    </message>
    <message>
        <source>Toggle</source>
        <translation type="vanished">Växla</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <source>Hu&amp;e:</source>
        <translation type="vanished">Nya&amp;ns:</translation>
    </message>
    <message>
        <source>&amp;Sat:</source>
        <translation type="vanished">&amp;Mättnad:</translation>
    </message>
    <message>
        <source>&amp;Val:</source>
        <translation type="vanished">&amp;Ljushet:</translation>
    </message>
    <message>
        <source>&amp;Red:</source>
        <translation type="vanished">&amp;Röd:</translation>
    </message>
    <message>
        <source>&amp;Green:</source>
        <translation type="vanished">&amp;Grön:</translation>
    </message>
    <message>
        <source>Bl&amp;ue:</source>
        <translation type="vanished">Bl&amp;å:</translation>
    </message>
    <message>
        <source>A&amp;lpha channel:</source>
        <translation type="vanished">Alfa&amp;kanal:</translation>
    </message>
    <message>
        <source>&amp;Basic colors</source>
        <translation type="vanished">&amp;Basfärger</translation>
    </message>
    <message>
        <source>&amp;Custom colors</source>
        <translation type="vanished">&amp;Anpassade färger</translation>
    </message>
    <message>
        <source>&amp;Define Custom Colors &gt;&gt;</source>
        <translation type="obsolete">&amp;Definiera anpassade färger &gt;&gt;</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Avbryt</translation>
    </message>
    <message>
        <source>&amp;Add to Custom Colors</source>
        <translation type="vanished">&amp;Lägg till i anpassade färger</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">Välj färg</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="obsolete">Falskt</translation>
    </message>
    <message>
        <source>True</source>
        <translation type="obsolete">Sant</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Stäng</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Kunde inte ansluta</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Kunde inte rulla tillbaka transaktion</translation>
    </message>
    <message>
        <source>Unable to set autocommit</source>
        <translation type="vanished">Kunde inte ställa in automatisk verkställning</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Kunde inte förbereda frågesats</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation type="vanished">Kunde inte binda variabel</translation>
    </message>
    <message>
        <source>Unable to fetch record %1</source>
        <translation type="vanished">Kunde inte hämta posten %1</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation type="vanished">Kunde inte hämta nästa</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="vanished">Kunde inte hämta första</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <source>AM</source>
        <translation type="vanished">AM</translation>
    </message>
    <message>
        <source>am</source>
        <translation type="vanished">am</translation>
    </message>
    <message>
        <source>PM</source>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>pm</source>
        <translation type="vanished">pm</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <source>What&apos;s This?</source>
        <translation type="vanished">Vad är det här?</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Spara</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="obsolete">&amp;Spara</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Avbryt</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Avbryt</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Stäng</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="obsolete">&amp;Stäng</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Verkställ</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Återställ</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjälp</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation type="vanished">Spara inte</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="vanished">Förkasta</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Ja</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation type="vanished">Ja till &amp;alla</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Nej</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation type="vanished">N&amp;ej till alla</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation type="vanished">Spara alla</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="vanished">Avbryt</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Försök igen</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignorera</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation type="vanished">Återställ standardvärden</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <source>Name</source>
        <translation type="vanished">Namn</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Storlek</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="vanished">Sort</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="vanished">Typ</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation type="vanished">Ändringsdatum</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <source>Close</source>
        <translation type="obsolete">Stäng</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <source>More</source>
        <translation type="obsolete">Mer</translation>
    </message>
    <message>
        <source>Less</source>
        <translation type="obsolete">Mindre</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <source>Debug Message:</source>
        <translation type="vanished">Felsökningsmeddelande:</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="vanished">Varning:</translation>
    </message>
    <message>
        <source>Fatal Error:</source>
        <translation type="vanished">Ödesdigert fel:</translation>
    </message>
    <message>
        <source>&amp;Show this message again</source>
        <translation type="vanished">&amp;Visa detta meddelande igen</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Alla filer (*)</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Kataloger</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Öppna</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Spara</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation type="vanished">%1 finns redan. 
Vill du ersätta den?</translation>
    </message>
    <message>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation type="vanished">%1 
Filen hittades inte. 
Kontrollera att det korrekta filnamnet angavs.</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="vanished">Min dator</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Byt namn</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Ta bort</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation type="vanished">Visa &amp;dolda filer</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Tillbaka</translation>
    </message>
    <message>
        <source>Parent Directory</source>
        <translation type="vanished">Föräldrakatalog</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Listvy</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation type="vanished">Detaljerad vy</translation>
    </message>
    <message>
        <source>Files of type:</source>
        <translation type="vanished">Filer av typen:</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation type="vanished">Katalog:</translation>
    </message>
    <message>
        <source>
File not found.
Please verify the correct file name was given</source>
        <translation type="obsolete">
Filen hittades inte. 
Kontrollera att det korrekta filnamnet angavs</translation>
    </message>
    <message>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation type="vanished">%1 
Katalogen hittades inte. 
Kontrollera att det korrekta katalognamnet angavs.</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation type="vanished">\&quot;%1\&quot; är skrivskyddad. 
Vill du ta bort den ändå?</translation>
    </message>
    <message>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation type="vanished">Är du säker på att du vill ta bort \&quot;%1\&quot;?</translation>
    </message>
    <message>
        <source>Could not delete directory.</source>
        <translation type="vanished">Kunde inte ta bort katalogen.</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Alla filer (*.*)</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="vanished">Spara som</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation type="vanished">Enhet</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Fil</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Okänt</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation type="obsolete">Hitta katalog</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Framåt</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="obsolete">Ny mapp</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation type="obsolete">Fil&amp;namn:</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation type="obsolete">Skapa ny mapp</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Namn</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Storlek</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="obsolete">Sort</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation type="obsolete">Ändringsdatum</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="obsolete">Min dator</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <source>&amp;Font</source>
        <translation type="vanished">&amp;Typsnitt</translation>
    </message>
    <message>
        <source>Font st&amp;yle</source>
        <translation type="vanished">T&amp;ypsnittsstil</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Storlek</translation>
    </message>
    <message>
        <source>Effects</source>
        <translation type="vanished">Effekter</translation>
    </message>
    <message>
        <source>Stri&amp;keout</source>
        <translation type="vanished">Genomstru&amp;ken</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation type="vanished">&amp;Understruken</translation>
    </message>
    <message>
        <source>Sample</source>
        <translation type="vanished">Test</translation>
    </message>
    <message>
        <source>Wr&amp;iting System</source>
        <translation type="vanished">Skr&amp;ivsystem</translation>
    </message>
    <message>
        <source>Select Font</source>
        <translation type="vanished">Välj typsnitt</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <source>Not connected</source>
        <translation type="vanished">Inte ansluten</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation type="vanished">Värden %1 hittades inte</translation>
    </message>
    <message>
        <source>Connection refused to host %1</source>
        <translation type="vanished">Anslutningen till värden %1 vägrades</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation type="vanished">Ansluten till värden %1</translation>
    </message>
    <message>
        <source>Connection refused for data connection</source>
        <translation type="vanished">Anslutning vägrades för dataanslutning</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Okänt fel</translation>
    </message>
    <message>
        <source>Connecting to host failed:
%1</source>
        <translation type="vanished">Anslutning till värden misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Login failed:
%1</source>
        <translation type="vanished">Inloggning misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Listing directory failed:
%1</source>
        <translation type="vanished">Listning av katalogen misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Changing directory failed:
%1</source>
        <translation type="vanished">Byte av katalog misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Downloading file failed:
%1</source>
        <translation type="vanished">Nedladdningen av filen misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Uploading file failed:
%1</source>
        <translation type="vanished">Uppladdningen av filen misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Removing file failed:
%1</source>
        <translation type="vanished">Borttagning av filen misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Creating directory failed:
%1</source>
        <translation type="vanished">Skapandet av katalogen misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Removing directory failed:
%1</source>
        <translation type="vanished">Borttagning av katalogen misslyckades: 
%1</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation type="vanished">Anslutningen stängd</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation type="vanished">Värden %1 hittades</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation type="vanished">Anslutningen till %1 stängdes</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation type="vanished">Värden hittades</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation type="vanished">Ansluten till värden</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation type="vanished">LTR</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Okänt fel</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Värden hittades inte</translation>
    </message>
    <message>
        <source>Unknown address type</source>
        <translation type="vanished">Okänd adresstyp</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Okänt fel</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Okänt fel</translation>
    </message>
    <message>
        <source>Request aborted</source>
        <translation type="vanished">Begäran avbröts</translation>
    </message>
    <message>
        <source>No server set to connect to</source>
        <translation type="vanished">Ingen server inställd att ansluta till</translation>
    </message>
    <message>
        <source>Wrong content length</source>
        <translation type="vanished">Fel innehållslängd</translation>
    </message>
    <message>
        <source>Server closed connection unexpectedly</source>
        <translation type="vanished">Servern stängde oväntat anslutningen</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Anslutningen nekades</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation type="vanished">Värden %1 hittades inte</translation>
    </message>
    <message>
        <source>HTTP request failed</source>
        <translation type="vanished">HTTP-begäran misslyckades</translation>
    </message>
    <message>
        <source>Invalid HTTP response header</source>
        <translation type="vanished">Ogiltig HTTP-svarshuvud</translation>
    </message>
    <message>
        <source>Invalid HTTP chunked body</source>
        <translation type="vanished">Ogiltig HTTP chunked body</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation type="vanished">Värden %1 hittades</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation type="vanished">Ansluten till värden %1</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation type="vanished">Anslutningen till %1 stängdes</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation type="vanished">Värden hittades</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation type="vanished">Ansluten till värd</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation type="vanished">Anslutningen stängd</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Fel vid öppning av databas</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation type="vanished">Kunde inte starta transaktion</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Kunde inte rulla tillbaka transaktion</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <source>Unable to create BLOB</source>
        <translation type="vanished">Kunde inte skapa BLOB</translation>
    </message>
    <message>
        <source>Unable to write BLOB</source>
        <translation type="vanished">Kunde inte skriva BLOB</translation>
    </message>
    <message>
        <source>Unable to open BLOB</source>
        <translation type="vanished">Kunde inte öppna BLOB</translation>
    </message>
    <message>
        <source>Unable to read BLOB</source>
        <translation type="vanished">Kunde inte läsa BLOB</translation>
    </message>
    <message>
        <source>Could not find array</source>
        <translation type="vanished">Kunde inte hitta kedja</translation>
    </message>
    <message>
        <source>Could not get array data</source>
        <translation type="vanished">Kunde inte få kedjedata</translation>
    </message>
    <message>
        <source>Could not get query info</source>
        <translation type="vanished">Kunde inte gå frågesatsinformation</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation type="vanished">Kunde inte starta transaktion</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Could not allocate statement</source>
        <translation type="vanished">Kunde inte allokera frågesats</translation>
    </message>
    <message>
        <source>Could not prepare statement</source>
        <translation type="vanished">Kunde inte förbereda frågesats</translation>
    </message>
    <message>
        <source>Could not describe input statement</source>
        <translation type="vanished">Kunde inte beskriva inmatningsfrågesats</translation>
    </message>
    <message>
        <source>Could not describe statement</source>
        <translation type="vanished">Kunde inte beskriva frågesats</translation>
    </message>
    <message>
        <source>Unable to close statement</source>
        <translation type="vanished">Kunde inte stänga frågesats</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
    <message>
        <source>Could not fetch next item</source>
        <translation type="vanished">Kunde inte hämta nästa post</translation>
    </message>
    <message>
        <source>Could not get statement info</source>
        <translation type="vanished">Kunde inte få frågesatsinformation</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Åtkomst nekad</translation>
    </message>
    <message>
        <source>Too many open files</source>
        <translation type="vanished">För många öppna filer</translation>
    </message>
    <message>
        <source>No such file or directory</source>
        <translation type="vanished">Ingen sådan fil eller katalog</translation>
    </message>
    <message>
        <source>No space left on device</source>
        <translation type="vanished">Inget ledigt utrymme på enheten</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Okänt fel</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <source>XIM</source>
        <translation type="vanished">XIM</translation>
    </message>
    <message>
        <source>XIM input method</source>
        <translation type="vanished">XIM-inmatningsmetod</translation>
    </message>
    <message>
        <source>Windows input method</source>
        <translation type="vanished">Windows-inmatningsmetod</translation>
    </message>
    <message>
        <source>Mac OS X input method</source>
        <translation type="vanished">Mac OS X-inmatningsmetod</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys: Kan inte läsa in %1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys: Kan inte läsa ur %1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: Symbolen &quot;%1&quot; är inte definierad i %2 (%3)</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="obsolete">Okänt fel</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Ångra</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Gör om</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Klipp &amp;ut</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopiera</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">Klistra &amp;in</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Ta bort</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Markera alla</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <source>Unable to open database &apos;</source>
        <translation type="vanished">Kunde inte öppna databasen \&quot;</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Kunde inte ansluta</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Kunde inte påbörja transaktion</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Kunde inte rulla tillbaka transaktion</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <source>Unable to fetch data</source>
        <translation type="vanished">Kunde inte hämta data</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
    <message>
        <source>Unable to store result</source>
        <translation type="vanished">Kunde inte lagra resultat</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Kunde inte förbereda frågesats</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Kunde inte återställa frågesats</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation type="vanished">Kunde inte binda värde</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
    <message>
        <source>Unable to bind outvalues</source>
        <translation type="vanished">Kunde inte binda utvärden</translation>
    </message>
    <message>
        <source>Unable to store statement results</source>
        <translation type="vanished">Kunde inte lagra resultat från frågesats</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <source>%1 - [%2]</source>
        <translation type="obsolete">%1 - [%2]</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Stäng</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="obsolete">Minimera</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation type="obsolete">Återställ nedåt</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="obsolete">Åte&amp;rställ</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="obsolete">&amp;Flytta</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="obsolete">&amp;Storlek</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="obsolete">Mi&amp;nimera</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="obsolete">Ma&amp;ximera</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation type="obsolete">Stanna kvar övers&amp;t</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="obsolete">&amp;Stäng</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="obsolete">Maximera</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjälp</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="obsolete">Meny</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Stäng</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation type="vanished">Kör</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">Om Qt</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjälp</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">Om Qt</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Detta program använder Qt version %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qtopia Core.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;Om Qt&lt;/h3&gt;%1&lt;p&gt;Qt är ett C++-verktygssamling för utveckling av krossplattformsprogram.&lt;/p&gt;&lt;p&gt;Qt tillhandahåller portabilitet för samma källkod mellan MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, och alla andra stora kommersiella Unix-varianter. Qt finns också tillgängligt för inbäddade enheter som Qtopia Core.&lt;/p&gt;&lt;p&gt;Qt är en produkt från Trolltech. Se &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; för mer information.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Show Details...</source>
        <translation type="vanished">Visa detaljer...</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation type="vanished">Dölj detaljer,,,</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <source>Select IM</source>
        <translation type="vanished">Välj inmatningsmetod</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <source>Multiple input method switcher</source>
        <translation type="vanished">Växlare för flera inmatningsmetoder</translation>
    </message>
    <message>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation type="vanished">Växlare för flera inmatningsmetoder som använder sammanhangsmenyn för textwidgar</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <source>The remote host closed the connection</source>
        <translation type="vanished">Fjärrvärden stängde anslutningen</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="vanished">Tidsgräns för nätverksåtgärd överstegs</translation>
    </message>
    <message>
        <source>Out of resources</source>
        <translation type="vanished">Slut på resurser</translation>
    </message>
    <message>
        <source>Unsupported socket operation</source>
        <translation type="vanished">Uttagsåtgärden stöds inte</translation>
    </message>
    <message>
        <source>Protocol type not supported</source>
        <translation type="vanished">Protokolltypen stöds inte</translation>
    </message>
    <message>
        <source>Invalid socket descriptor</source>
        <translation type="vanished">Ogiltig uttagsbeskrivare</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="vanished">Nätverket är inte nåbart</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Åtkomst nekad</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="vanished">Tidsgränsen för anslutning överstegs</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Anslutningen vägrades</translation>
    </message>
    <message>
        <source>The bound address is already in use</source>
        <translation type="vanished">Bindningsadress används redan</translation>
    </message>
    <message>
        <source>The address is not available</source>
        <translation type="vanished">Adressen är inte tillgänglig</translation>
    </message>
    <message>
        <source>The address is protected</source>
        <translation type="vanished">Adressen är skyddad</translation>
    </message>
    <message>
        <source>Unable to send a message</source>
        <translation type="vanished">Kunde inte skicka ett meddelande</translation>
    </message>
    <message>
        <source>Unable to receive a message</source>
        <translation type="vanished">Kunde inte ta emot ett meddelande</translation>
    </message>
    <message>
        <source>Unable to write</source>
        <translation type="vanished">Kunde inte skriva</translation>
    </message>
    <message>
        <source>Network error</source>
        <translation type="vanished">Nätverksfel</translation>
    </message>
    <message>
        <source>Another socket is already listening on the same port</source>
        <translation type="vanished">Ett annat uttag lyssnar redan på samma port</translation>
    </message>
    <message>
        <source>Unable to initialize non-blocking socket</source>
        <translation type="vanished">Kunde inte initiera icke-blockerande uttag</translation>
    </message>
    <message>
        <source>Unable to initialize broadcast socket</source>
        <translation type="vanished">Kunde inte initiera uttag för broadcast</translation>
    </message>
    <message>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation type="vanished">Försök att använda IPv6-uttag på en plattform som saknar IPv6-stöd</translation>
    </message>
    <message>
        <source>Host unreachable</source>
        <translation type="vanished">Värden är inte nåbar</translation>
    </message>
    <message>
        <source>Datagram was too large to send</source>
        <translation type="vanished">Datagram för för stor för att skicka</translation>
    </message>
    <message>
        <source>Operation on non-socket</source>
        <translation type="vanished">Åtgärd på icke-uttag</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Okänt fel</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <source>Unable to logon</source>
        <translation type="vanished">Kunde inte logga in</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="obsolete">Kunde inte påbörja transaktion</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="obsolete">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="obsolete">Kunde inte rulla tillbaka transaktion</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <source>Unable to bind column for batch execute</source>
        <translation type="vanished">Kunde inte binda kolumn för satskörning</translation>
    </message>
    <message>
        <source>Unable to execute batch statement</source>
        <translation type="vanished">Kunde inte köra satsfråga</translation>
    </message>
    <message>
        <source>Unable to goto next</source>
        <translation type="vanished">Kunde inte gå till nästa</translation>
    </message>
    <message>
        <source>Unable to alloc statement</source>
        <translation type="vanished">Kunde inte allokera frågesats</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Kunde inte förbereda frågesats</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation type="vanished">Kunde inte binda värde</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">Kunde inte köra \&quot;select\&quot;-frågesats</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Kunde inte ansluta</translation>
    </message>
    <message>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation type="vanished">Kunde inte ansluta - Drivrutinen har inte stöd för all nödvändig funktionalitet</translation>
    </message>
    <message>
        <source>Unable to disable autocommit</source>
        <translation type="vanished">Kunde inte inaktivera automatisk verkställning</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Kunde inte rulla tillbaka transaktion</translation>
    </message>
    <message>
        <source>Unable to enable autocommit</source>
        <translation type="vanished">Kunde inte aktivera automatisk verkställning</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation type="vanished">QODBCResult::reset: Kunde inte ställa in \&quot;SQL_CURSOR_STATIC\&quot; som frågesatsattribut. Kontrollera konfigurationen för din ODBC-drivrutin</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation type="vanished">Kunde inte hämta nästa</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Kunde inte förbereda frågesats</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation type="vanished">Kunde inte binda variabel</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="obsolete">Kunde inte hämta första</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Home</source>
        <translation type="obsolete">Home</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Namn</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Värde</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Kunde inte ansluta</translation>
    </message>
    <message>
        <source>Could not begin transaction</source>
        <translation type="vanished">Kunde inte påbörja transaktion</translation>
    </message>
    <message>
        <source>Could not commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Could not rollback transaction</source>
        <translation type="vanished">Kunde inte rulla tillbaka transaktion</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <source>Unable to create query</source>
        <translation type="vanished">Kunde inte skapa fråga</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="obsolete">Kunde inte förbereda frågesats</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <source>Page size:</source>
        <translation type="obsolete">Sidstorlek:</translation>
    </message>
    <message>
        <source>Paper source:</source>
        <translation type="obsolete">Papperskälla:</translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="obsolete">Stående</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="obsolete">Liggande</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <source>Unknown error</source>
        <translation type="obsolete">Okänt fel</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>locally connected</source>
        <translation type="vanished">lokalt ansluten</translation>
    </message>
    <message>
        <source>Aliases: %1</source>
        <translation type="vanished">Alias: %1</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">okänt</translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="obsolete">Stående</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="obsolete">Liggande</translation>
    </message>
    <message>
        <source>A0 (841 x 1189 mm)</source>
        <translation type="vanished">A0 (841 x 1189 mm)</translation>
    </message>
    <message>
        <source>A1 (594 x 841 mm)</source>
        <translation type="vanished">A1 (594 x 841 mm)</translation>
    </message>
    <message>
        <source>A2 (420 x 594 mm)</source>
        <translation type="vanished">A2 (420 x 594 mm)</translation>
    </message>
    <message>
        <source>A3 (297 x 420 mm)</source>
        <translation type="vanished">A3 (297 x 420 mm)</translation>
    </message>
    <message>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation type="vanished">A4 (210 x 297 mm, 8.26 x 11.7 tum)</translation>
    </message>
    <message>
        <source>A5 (148 x 210 mm)</source>
        <translation type="vanished">A5 (148 x 210 mm)</translation>
    </message>
    <message>
        <source>A6 (105 x 148 mm)</source>
        <translation type="vanished">A6 (105 x 148 mm)</translation>
    </message>
    <message>
        <source>A7 (74 x 105 mm)</source>
        <translation type="vanished">A7 (74 x 105 mm)</translation>
    </message>
    <message>
        <source>A8 (52 x 74 mm)</source>
        <translation type="vanished">A8 (52 x 74 mm)</translation>
    </message>
    <message>
        <source>A9 (37 x 52 mm)</source>
        <translation type="vanished">A9 (37 x 52 mm)</translation>
    </message>
    <message>
        <source>B0 (1000 x 1414 mm)</source>
        <translation type="vanished">B0 (1000 x 1414 mm)</translation>
    </message>
    <message>
        <source>B1 (707 x 1000 mm)</source>
        <translation type="vanished">B1 (707 x 1000 mm)</translation>
    </message>
    <message>
        <source>B2 (500 x 707 mm)</source>
        <translation type="vanished">B2 (500 x 707 mm)</translation>
    </message>
    <message>
        <source>B3 (353 x 500 mm)</source>
        <translation type="vanished">B3 (353 x 500 mm)</translation>
    </message>
    <message>
        <source>B4 (250 x 353 mm)</source>
        <translation type="vanished">B4 (250 x 353 mm)</translation>
    </message>
    <message>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation type="vanished">B5 (176 x 250 mm, 6.93 x 9.84 tum)</translation>
    </message>
    <message>
        <source>B6 (125 x 176 mm)</source>
        <translation type="vanished">B6 (125 x 176 mm)</translation>
    </message>
    <message>
        <source>B7 (88 x 125 mm)</source>
        <translation type="vanished">B7 (88 x 125 mm)</translation>
    </message>
    <message>
        <source>B8 (62 x 88 mm)</source>
        <translation type="vanished">B8 (62 x 88 mm)</translation>
    </message>
    <message>
        <source>B9 (44 x 62 mm)</source>
        <translation type="vanished">B9 (44 x 62 mm)</translation>
    </message>
    <message>
        <source>B10 (31 x 44 mm)</source>
        <translation type="vanished">B10 (31 x 44 mm)</translation>
    </message>
    <message>
        <source>C5E (163 x 229 mm)</source>
        <translation type="vanished">C5E (163 x 229 mm)</translation>
    </message>
    <message>
        <source>DLE (110 x 220 mm)</source>
        <translation type="vanished">DLE (110 x 220 mm)</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation type="vanished">Executive (7.5 x 10 tum, 191 x 254 mm)</translation>
    </message>
    <message>
        <source>Folio (210 x 330 mm)</source>
        <translation type="vanished">Folio (210 x 330 mm)</translation>
    </message>
    <message>
        <source>Ledger (432 x 279 mm)</source>
        <translation type="vanished">Ledger (432 x 279 mm)</translation>
    </message>
    <message>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation type="vanished">Legal (8.5 x 14 tum, 216 x 356 mm)</translation>
    </message>
    <message>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation type="vanished">Letter (8.5 x 11 tum, 216 x 279 mm)</translation>
    </message>
    <message>
        <source>Tabloid (279 x 432 mm)</source>
        <translation type="vanished">Tabloid (279 x 432 mm)</translation>
    </message>
    <message>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation type="vanished">US Common #10 Envelope (105 x 241 mm)</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Avbryt</translation>
    </message>
    <message>
        <source>Page size:</source>
        <translation type="obsolete">Sidstorlek:</translation>
    </message>
    <message>
        <source>Orientation:</source>
        <translation type="obsolete">Orientering:</translation>
    </message>
    <message>
        <source>Paper source:</source>
        <translation type="obsolete">Papperskälla:</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Skriv ut</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Fil</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="obsolete">Skrivare</translation>
    </message>
    <message>
        <source>Print To File ...</source>
        <translation type="vanished">Skriv ut till fil ...</translation>
    </message>
    <message>
        <source>Print dialog</source>
        <translation type="obsolete">Utskriftsdialog</translation>
    </message>
    <message>
        <source>Paper format</source>
        <translation type="obsolete">Pappersformat</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="obsolete">Storlek:</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Egenskaper</translation>
    </message>
    <message>
        <source>Printer info:</source>
        <translation type="obsolete">Skrivarinformation:</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="obsolete">Bläddra</translation>
    </message>
    <message>
        <source>Print to file</source>
        <translation type="obsolete">Skriv ut till fil</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="vanished">Skriv ut intervall</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="vanished">Skriv ut alla</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="obsolete">Sidor från</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">till</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="obsolete">Val</translation>
    </message>
    <message>
        <source>Copies</source>
        <translation type="obsolete">Kopior</translation>
    </message>
    <message>
        <source>Number of copies:</source>
        <translation type="obsolete">Antal kopior:</translation>
    </message>
    <message>
        <source>Collate</source>
        <translation type="obsolete">Sortera</translation>
    </message>
    <message>
        <source>Print last page first</source>
        <translation type="obsolete">Skriv ut sista sidan först</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="obsolete">Annat</translation>
    </message>
    <message>
        <source>Print in color if available</source>
        <translation type="obsolete">Skriv ut i färg om möjligt</translation>
    </message>
    <message>
        <source>Double side printing</source>
        <translation type="obsolete">Dubbelsidig utskrift</translation>
    </message>
    <message>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation type="vanished">Filen %1 är inte skrivbar. 
Välj ett annat filnamn.</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation type="vanished">%1 finns redan. 
Vill du skriva över den?</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <source>Portrait</source>
        <translation type="obsolete">Stående</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="obsolete">Liggande</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Stäng</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>PPD Properties</source>
        <translation type="obsolete">PPD-egenskaper</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Spara</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <source>Copies</source>
        <translation type="obsolete">Kopior</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="obsolete">Skriv ut intervall</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="obsolete">Skriv ut alla</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="obsolete">Sidor från</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">till</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="obsolete">Val</translation>
    </message>
    <message>
        <source>Collate</source>
        <translation type="obsolete">Sortera</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <source>Printer</source>
        <translation type="obsolete">Skrivare</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Avbryt</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <source>Check</source>
        <translation type="vanished">Kryssa</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <source>no error occurred</source>
        <translation type="vanished">inga fel inträffade</translation>
    </message>
    <message>
        <source>disabled feature used</source>
        <translation type="vanished">inaktiverad funktion används</translation>
    </message>
    <message>
        <source>bad char class syntax</source>
        <translation type="vanished">felaktig teckenklasssyntax</translation>
    </message>
    <message>
        <source>bad lookahead syntax</source>
        <translation type="vanished">felaktig seframåtsyntax</translation>
    </message>
    <message>
        <source>bad repetition syntax</source>
        <translation type="vanished">felaktig upprepningssyntax</translation>
    </message>
    <message>
        <source>invalid octal value</source>
        <translation type="vanished">ogiltigt oktalt värde</translation>
    </message>
    <message>
        <source>missing left delim</source>
        <translation type="vanished">saknar vänster avgränsare</translation>
    </message>
    <message>
        <source>unexpected end</source>
        <translation type="vanished">oväntat slut</translation>
    </message>
    <message>
        <source>met internal limit</source>
        <translation type="vanished">nådde intern gräns</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <source>Error to open database</source>
        <translation type="vanished">Fel vid öppning av databas</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Kunde inte påbörja transaktion</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback Transaction</source>
        <translation type="vanished">Kunde inte rulla tillbaka transaktion</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <source>Unable to fetch results</source>
        <translation type="vanished">Kunde inte hämta resultat</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Fel vid öppning av databas</translation>
    </message>
    <message>
        <source>Error closing database</source>
        <translation type="vanished">Fel vid stängning av databas</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Kunde inte påbörja transaktion</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Kunde inte verkställa transaktion</translation>
    </message>
    <message>
        <source>Unable to roll back transaction</source>
        <translation type="obsolete">Kunde inte rulla tillbaka transaktion</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="obsolete">Kunde inte rulla tillbaka transaktion</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <source>Unable to fetch row</source>
        <translation type="vanished">Kunde inte hämta rad</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Kunde inte köra frågesats</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Kunde inte återställa frågesats</translation>
    </message>
    <message>
        <source>Unable to bind parameters</source>
        <translation type="vanished">Kunde inte binda parametrar</translation>
    </message>
    <message>
        <source>Parameter count mismatch</source>
        <translation type="vanished">Parameterantal stämmer inte</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <source>Scroll here</source>
        <translation type="vanished">Rulla här</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="vanished">Vänsterkant</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="vanished">Överkant</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="vanished">Högerkant</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="vanished">Nederkant</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="vanished">Sida vänster</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Sida uppåt</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Sida höger</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="vanished">Sida nedåt</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="vanished">Rulla vänster</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="vanished">Rulla uppåt</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="vanished">Rulla höger</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="vanished">Rulla nedåt</translation>
    </message>
    <message>
        <source>Line up</source>
        <translation type="vanished">Rada upp</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Position</translation>
    </message>
    <message>
        <source>Line down</source>
        <translation type="vanished">Rad nedåt</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <source>Space</source>
        <translation type="vanished">Mellanslag</translation>
    </message>
    <message>
        <source>Esc</source>
        <translation type="vanished">Esc</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Tab</translation>
    </message>
    <message>
        <source>Backtab</source>
        <translation type="vanished">Backtab</translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="vanished">Backsteg</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">Return</translation>
    </message>
    <message>
        <source>Enter</source>
        <translation type="vanished">Enter</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="vanished">Ins</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">Pause</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Print</translation>
    </message>
    <message>
        <source>SysReq</source>
        <translation type="vanished">SysReq</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Home</translation>
    </message>
    <message>
        <source>End</source>
        <translation type="vanished">End</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="vanished">Vänster</translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="vanished">Upp</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="vanished">Höger</translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="vanished">Ned</translation>
    </message>
    <message>
        <source>PgUp</source>
        <translation type="vanished">PgUp</translation>
    </message>
    <message>
        <source>PgDown</source>
        <translation type="vanished">PgDown</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation type="vanished">CapsLock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation type="vanished">NumLock</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation type="vanished">ScrollLock</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Meny</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjälp</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Bakåt</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Framåt</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Volume Down</source>
        <translation type="vanished">Sänk volym</translation>
    </message>
    <message>
        <source>Volume Mute</source>
        <translation type="vanished">Volym tyst</translation>
    </message>
    <message>
        <source>Volume Up</source>
        <translation type="vanished">Höj volym</translation>
    </message>
    <message>
        <source>Bass Boost</source>
        <translation type="vanished">Förstärk bas</translation>
    </message>
    <message>
        <source>Bass Up</source>
        <translation type="vanished">Höj bas</translation>
    </message>
    <message>
        <source>Bass Down</source>
        <translation type="vanished">Sänk bas</translation>
    </message>
    <message>
        <source>Treble Up</source>
        <translation type="vanished">Höj diskant</translation>
    </message>
    <message>
        <source>Treble Down</source>
        <translation type="vanished">Sänk diskant</translation>
    </message>
    <message>
        <source>Media Play</source>
        <translation type="vanished">Media spela upp</translation>
    </message>
    <message>
        <source>Media Stop</source>
        <translation type="vanished">Media stopp</translation>
    </message>
    <message>
        <source>Media Previous</source>
        <translation type="vanished">Media föregående</translation>
    </message>
    <message>
        <source>Media Next</source>
        <translation type="vanished">Media nästa</translation>
    </message>
    <message>
        <source>Media Record</source>
        <translation type="vanished">Media spela in</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Favoriter</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Sök</translation>
    </message>
    <message>
        <source>Standby</source>
        <translation type="vanished">Avvakta</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="vanished">Öppna url</translation>
    </message>
    <message>
        <source>Launch Mail</source>
        <translation type="vanished">Starta e-post</translation>
    </message>
    <message>
        <source>Launch Media</source>
        <translation type="vanished">Starta media</translation>
    </message>
    <message>
        <source>Launch (0)</source>
        <translation type="vanished">Starta (0)</translation>
    </message>
    <message>
        <source>Launch (1)</source>
        <translation type="vanished">Starta (1)</translation>
    </message>
    <message>
        <source>Launch (2)</source>
        <translation type="vanished">Starta (2)</translation>
    </message>
    <message>
        <source>Launch (3)</source>
        <translation type="vanished">Starta (3)</translation>
    </message>
    <message>
        <source>Launch (4)</source>
        <translation type="vanished">Starta (4)</translation>
    </message>
    <message>
        <source>Launch (5)</source>
        <translation type="vanished">Starta (5)</translation>
    </message>
    <message>
        <source>Launch (6)</source>
        <translation type="vanished">Starta (6)</translation>
    </message>
    <message>
        <source>Launch (7)</source>
        <translation type="vanished">Starta (7)</translation>
    </message>
    <message>
        <source>Launch (8)</source>
        <translation type="vanished">Starta (8)</translation>
    </message>
    <message>
        <source>Launch (9)</source>
        <translation type="vanished">Starta (9)</translation>
    </message>
    <message>
        <source>Launch (A)</source>
        <translation type="vanished">Starta (A)</translation>
    </message>
    <message>
        <source>Launch (B)</source>
        <translation type="vanished">Starta (B)</translation>
    </message>
    <message>
        <source>Launch (C)</source>
        <translation type="vanished">Starta (C)</translation>
    </message>
    <message>
        <source>Launch (D)</source>
        <translation type="vanished">Starta (D)</translation>
    </message>
    <message>
        <source>Launch (E)</source>
        <translation type="vanished">Starta (E)</translation>
    </message>
    <message>
        <source>Launch (F)</source>
        <translation type="vanished">Starta (F)</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">Print Screen</translation>
    </message>
    <message>
        <source>Page Up</source>
        <translation type="vanished">Page Up</translation>
    </message>
    <message>
        <source>Page Down</source>
        <translation type="vanished">Page Down</translation>
    </message>
    <message>
        <source>Caps Lock</source>
        <translation type="vanished">Caps Lock</translation>
    </message>
    <message>
        <source>Num Lock</source>
        <translation type="vanished">Num Lock</translation>
    </message>
    <message>
        <source>Number Lock</source>
        <translation type="vanished">Number Lock</translation>
    </message>
    <message>
        <source>Scroll Lock</source>
        <translation type="vanished">Scroll Lock</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Insert</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Escape</source>
        <translation type="vanished">Escape</translation>
    </message>
    <message>
        <source>System Request</source>
        <translation type="vanished">System Request</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Välj</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Nej</translation>
    </message>
    <message>
        <source>Context1</source>
        <translation type="vanished">Sammanhang1</translation>
    </message>
    <message>
        <source>Context2</source>
        <translation type="vanished">Sammanhang2</translation>
    </message>
    <message>
        <source>Context3</source>
        <translation type="vanished">Sammanhang3</translation>
    </message>
    <message>
        <source>Context4</source>
        <translation type="vanished">Sammanhang4</translation>
    </message>
    <message>
        <source>Call</source>
        <translation type="vanished">Ring upp</translation>
    </message>
    <message>
        <source>Hangup</source>
        <translation type="vanished">Lägg på</translation>
    </message>
    <message>
        <source>Flip</source>
        <translation type="vanished">Vänd</translation>
    </message>
    <message>
        <source>Ctrl</source>
        <translation type="vanished">Ctrl</translation>
    </message>
    <message>
        <source>Shift</source>
        <translation type="vanished">Shift</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation type="vanished">Alt</translation>
    </message>
    <message>
        <source>Meta</source>
        <translation type="vanished">Meta</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <source>F%1</source>
        <translation type="vanished">F%1</translation>
    </message>
    <message>
        <source>Home Page</source>
        <translation type="vanished">Hemsida</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <source>Page left</source>
        <translation type="vanished">Sida vänster</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Sida uppåt</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Position</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Sida höger</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="vanished">Sida nedåt</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">Tidsgräns för Socks5 överstigen vid anslutningen till socks-server</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="obsolete">Tidsgräns för nätverksåtgärd överstegs</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <source>More</source>
        <translation type="vanished">Mer</translation>
    </message>
    <message>
        <source>Less</source>
        <translation type="vanished">Mindre</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <source>Delete</source>
        <translation type="vanished">Ta bort</translation>
    </message>
    <message>
        <source>Delete this record?</source>
        <translation type="vanished">Ta bort denna post?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Nej</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Infoga</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Save edits?</source>
        <translation type="vanished">Spara redigeringar?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Avbryt</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Bekräfta</translation>
    </message>
    <message>
        <source>Cancel your edits?</source>
        <translation type="vanished">Avbryt dina redigeringar?</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <source>Unable to open connection</source>
        <translation type="vanished">Kunde inte öppna anslutning</translation>
    </message>
    <message>
        <source>Unable to use database</source>
        <translation type="vanished">Kunde inte använda databasen</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <source>Scroll Left</source>
        <translation type="vanished">Rulla vänster</translation>
    </message>
    <message>
        <source>Scroll Right</source>
        <translation type="vanished">Rulla höger</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <source>Socket operation unsupported</source>
        <translation type="obsolete">Uttagsåtgärd stöds inte</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Ångra</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Gör om</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Klipp u&amp;t</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopiera</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation type="vanished">Kopiera &amp;länkplats</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">Klistra &amp;in</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Ta bort</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Markera alla</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <source>Press</source>
        <translation type="vanished">Tryck</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Öppna</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <source>This platform does not support IPv6</source>
        <translation type="vanished">Denna plattform saknar stöd för IPv6</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <source>Undo</source>
        <translation type="vanished">Ångra</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="vanished">Gör om</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <source>&lt;empty&gt;</source>
        <translation type="vanished">&lt;tom&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <source>Undo</source>
        <translation type="vanished">Ångra</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="vanished">Gör om</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <source>LRM Left-to-right mark</source>
        <translation type="vanished">U+200E</translation>
    </message>
    <message>
        <source>RLM Right-to-left mark</source>
        <translation type="vanished">U+200F</translation>
    </message>
    <message>
        <source>ZWJ Zero width joiner</source>
        <translation type="vanished">U+200D</translation>
    </message>
    <message>
        <source>ZWNJ Zero width non-joiner</source>
        <translation type="vanished">U+200C</translation>
    </message>
    <message>
        <source>ZWSP Zero width space</source>
        <translation type="vanished">U+200B</translation>
    </message>
    <message>
        <source>LRE Start of left-to-right embedding</source>
        <translation type="vanished">U+202A</translation>
    </message>
    <message>
        <source>RLE Start of right-to-left embedding</source>
        <translation type="vanished">U+202B</translation>
    </message>
    <message>
        <source>LRO Start of left-to-right override</source>
        <translation type="vanished">U+202D</translation>
    </message>
    <message>
        <source>RLO Start of right-to-left override</source>
        <translation type="vanished">U+202E</translation>
    </message>
    <message>
        <source>PDF Pop directional formatting</source>
        <translation type="vanished">U+202C</translation>
    </message>
    <message>
        <source>Insert Unicode control character</source>
        <translation type="vanished">Infoga unicode-kontrolltecken</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation type="obsolete">Återställ</translation>
    </message>
    <message>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation type="obsolete">Stoppa</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation type="obsolete">Ignorera</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation type="obsolete">Ignorera</translation>
    </message>
    <message>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation type="obsolete">Okänt</translation>
    </message>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">Rulla här</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">Vänsterkant</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">Överkant</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">Högerkant</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">Nederkant</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">Sida vänster</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">Sida uppåt</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">Sida höger</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">Sida nedåt</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">Rulla vänster</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">Rulla uppåt</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">Rulla höger</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">Rulla nedåt</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <source>What&apos;s This?</source>
        <translation type="vanished">Vad är det här?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <source>*</source>
        <translation type="vanished">*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjälp</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation type="obsolete">&lt; Till&amp;baka</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation type="obsolete">&amp;Färdig</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Avbryt</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Hjälp</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="obsolete">&amp;Nästa &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">Åte&amp;rställ</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="vanished">&amp;Flytta</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Storlek</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">Mi&amp;nimera</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="vanished">Ma&amp;ximera</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">&amp;Stäng</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation type="vanished">Stanna kvar övers&amp;t</translation>
    </message>
    <message>
        <source>Sh&amp;ade</source>
        <translation type="vanished">Skugg&amp;a</translation>
    </message>
    <message>
        <source>%1 - [%2]</source>
        <translation type="vanished">%1 - [%2]</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Minimera</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation type="vanished">Återställ nedåt</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Stäng</translation>
    </message>
    <message>
        <source>&amp;Unshade</source>
        <translation type="vanished">A&amp;vskugga</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <source>no error occurred</source>
        <translation type="vanished">inga fel inträffade</translation>
    </message>
    <message>
        <source>error triggered by consumer</source>
        <translation type="vanished">fel utlöstes av konsument</translation>
    </message>
    <message>
        <source>unexpected end of file</source>
        <translation type="vanished">oväntat slut på filen</translation>
    </message>
    <message>
        <source>more than one document type definition</source>
        <translation type="vanished">fler än en dokumenttypsdefinition</translation>
    </message>
    <message>
        <source>error occurred while parsing element</source>
        <translation type="vanished">fel inträffade vid tolkning av element</translation>
    </message>
    <message>
        <source>tag mismatch</source>
        <translation type="vanished">tagg stämmer inte</translation>
    </message>
    <message>
        <source>error occurred while parsing content</source>
        <translation type="vanished">fel inträffade vid tolkning av innehåll</translation>
    </message>
    <message>
        <source>unexpected character</source>
        <translation type="vanished">oväntat tecken</translation>
    </message>
    <message>
        <source>invalid name for processing instruction</source>
        <translation type="vanished">ogiltigt namn för behandlingsinstruktion</translation>
    </message>
    <message>
        <source>version expected while reading the XML declaration</source>
        <translation type="vanished">version förväntades vid läsning av XML-deklareringen</translation>
    </message>
    <message>
        <source>wrong value for standalone declaration</source>
        <translation type="vanished">fel värde för fristående deklarering</translation>
    </message>
    <message>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation type="vanished">kodningsdeklarering eller fristående deklarering förväntades vid läsning av XML-deklareringen</translation>
    </message>
    <message>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation type="vanished">fristående deklarering förväntades vid läsning av XML-deklarering</translation>
    </message>
    <message>
        <source>error occurred while parsing document type definition</source>
        <translation type="vanished">fel inträffade vid tolkning av dokumenttypsdefinition</translation>
    </message>
    <message>
        <source>letter is expected</source>
        <translation type="vanished">bokstav förväntades</translation>
    </message>
    <message>
        <source>error occurred while parsing comment</source>
        <translation type="vanished">fel inträffade vid tolkning av kommentar</translation>
    </message>
    <message>
        <source>error occurred while parsing reference</source>
        <translation type="vanished">fel inträffade vid tolkning av referens</translation>
    </message>
    <message>
        <source>internal general entity reference not allowed in DTD</source>
        <translation type="vanished">intern allmän entitetsreferens tillåts inte i DTD</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation type="vanished">extern tolkad allmän entitetsreferens tillåts inte i attributvärde</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation type="vanished">extern tolkad allmän entitetsreferens tillåts inte i DTD</translation>
    </message>
    <message>
        <source>unparsed entity reference in wrong context</source>
        <translation type="vanished">otolkad entitetsreferens i fel sammanhang</translation>
    </message>
    <message>
        <source>recursive entities</source>
        <translation type="vanished">rekursiva entiteter</translation>
    </message>
    <message>
        <source>error in the text declaration of an external entity</source>
        <translation type="vanished">fel i textdeklareringen av en extern entitet</translation>
    </message>
</context>
<context>
    <name>QtFrontend</name>
    <message>
        <location filename="../src/frontends/qtfrontend/qtfrontend.cpp" line="+137"/>
        <source>Create directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>You do not have the necessary permissions to run qStopMotion.
You need permission to create the .qstopmotion directory in your home directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Check Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>You do not have the necessary permissions to run qStopMotion.
You need permission to read, write and execute on the .qstopmotion directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+600"/>
        <source>&amp;Yes</source>
        <translation type="unfinished">&amp;Ja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;No</source>
        <translation type="unfinished">&amp;Nej</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Something caused qStopmotion to exit abnormally
last time it was run. Do you want to recover?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>qStopMotion - Recovered Project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordingTab</name>
    <message>
        <location filename="../src/frontends/qtfrontend/tooltabs/recordingtab.cpp" line="+298"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video Source:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Recording mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Single frame capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time-lapse capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Toggle camera on/off (C)&lt;/h4&gt; &lt;p&gt;Click this button to toggle the camera on and off&lt;/p&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Number of images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;h4&gt;Number of images&lt;/h4&gt; &lt;p&gt;By changing the value in this slidebar you can specify how many images backwards in the animation which should be mixed on top of the camera or if you are in playback mode: how many images to play. &lt;/p&gt; &lt;p&gt;By mixing the previous image(s) onto the camera you can more easily see how the next shot will be in relation to the other, therby making a smoother stop motion animation!&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Time-lapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>seconds before picture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Not Supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+341"/>
        <source>Seconds between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Minutes between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Hours between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Days between pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <location line="+6"/>
        <location line="+6"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-12"/>
        <source>No active project. Please create a new project or open an existing project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No scene and no take selected. Please select a scene and a take on the project tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No take selected. Please select a take on the project tab.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scene</name>
    <message>
        <location filename="../src/domain/animation/scene.cpp" line="+694"/>
        <location line="+66"/>
        <location line="+4"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-69"/>
        <location line="+70"/>
        <source>Can&apos;t remove sound file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Can&apos;t copy sound file!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartDialog</name>
    <message>
        <location filename="../src/frontends/qtfrontend/dialogs/startdialog.cpp" line="+62"/>
        <source>&lt;h2&gt;Create a new Project&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Start a new Project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>&lt;h2&gt;Open last Project&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Project path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>&lt;h2&gt;Open project file&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open and continue a existing project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Stäng</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/frontends/qtfrontend/timeline/timeline.cpp" line="+707"/>
        <source>Load images to time line</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/frontends/qtfrontend/toolbar.cpp" line="+175"/>
        <source>&lt;h4&gt;Degree of overlay&lt;/h4&gt;&lt;p&gt;Change the degree of superposition of the previous exposure to live video from camera&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&lt;h4&gt;Play animation (K, P)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;h4&gt;First frame of the take (J, Left)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>&lt;h4&gt;Previous frame (J, Left)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Next frame (L, Right)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;h4&gt;Last frame of the take (L, Right)&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&lt;h4&gt;Capture Frame (Space)&lt;/h4&gt; &lt;p&gt;Click on this button to &lt;em&gt;capture&lt;/em&gt; a frame from the camera an put it in the animation&lt;/p&gt; &lt;p&gt; This can also be done by pressing the &lt;b&gt;Space key&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Running animation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransformWidget</name>
    <message>
        <location filename="../src/frontends/qtfrontend/preferences/transformwidget.cpp" line="+90"/>
        <source>Below you can set which image transformation should be used for importing images to a new project. If you select clip a part of the image set also the adjustment for cliping.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Below you can set which image transformation should be used for importing images to the currently active project. If you select clip a part of the image set also the adjustment for cliping.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Transformation settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scale the whole image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clip a part of the image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom the image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Zoom settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom value (%):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureAdd</name>
    <message>
        <location filename="../src/domain/undo/undoexposureadd.cpp" line="+44"/>
        <source>Add exposure (%1,%2,%3) &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureInsert</name>
    <message>
        <location filename="../src/domain/undo/undoexposureinsert.cpp" line="+44"/>
        <source>Insert exposure (%1,%2,%3) &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureMove</name>
    <message>
        <location filename="../src/domain/undo/undoexposuremove.cpp" line="+45"/>
        <source>Move exposure (%1,%2,%3)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureRemove</name>
    <message>
        <location filename="../src/domain/undo/undoexposureremove.cpp" line="+42"/>
        <source>Remove exposure (%1,%2,%3)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoExposureSelect</name>
    <message>
        <location filename="../src/domain/undo/undoexposureselect.cpp" line="+40"/>
        <source>Select exposure (%1,%2,%3)--&gt;(%4,%5,%6)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoProjectNew</name>
    <message>
        <location filename="../src/domain/undo/undoprojectnew.cpp" line="+37"/>
        <source>New project &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoProjectOpen</name>
    <message>
        <location filename="../src/domain/undo/undoprojectopen.cpp" line="+35"/>
        <source>Open project &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneAdd</name>
    <message>
        <location filename="../src/domain/undo/undosceneadd.cpp" line="+36"/>
        <source>Add scene (%1) &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneInsert</name>
    <message>
        <location filename="../src/domain/undo/undosceneinsert.cpp" line="+37"/>
        <source>Insert scene (%1) &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneMove</name>
    <message>
        <location filename="../src/domain/undo/undoscenemove.cpp" line="+37"/>
        <source>Move scene (%1,%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneRemove</name>
    <message>
        <location filename="../src/domain/undo/undosceneremove.cpp" line="+36"/>
        <source>Remove scene (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoSceneSelect</name>
    <message>
        <location filename="../src/domain/undo/undosceneselect.cpp" line="+34"/>
        <source>Select scene (%1)--&gt;(%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeAdd</name>
    <message>
        <location filename="../src/domain/undo/undotakeadd.cpp" line="+39"/>
        <source>Add take (%1,%2) &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeInsert</name>
    <message>
        <location filename="../src/domain/undo/undotakeinsert.cpp" line="+40"/>
        <source>Insert take (%1,%2) &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeMove</name>
    <message>
        <location filename="../src/domain/undo/undotakemove.cpp" line="+39"/>
        <source>Move take (%1,%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeRemove</name>
    <message>
        <location filename="../src/domain/undo/undotakeremove.cpp" line="+38"/>
        <source>Remove take (%1,%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UndoTakeSelect</name>
    <message>
        <location filename="../src/domain/undo/undotakeselect.cpp" line="+37"/>
        <source>Select take (%1,%2)--&gt;(%3,%4)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
