/******************************************************************************
 *  Copyright (C) 2012-2020 by                                                *
 *    Ralf Lange (ralf.lange@longsoft.de)                                     *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the                             *
 *  Free Software Foundation, Inc.,                                           *
 *  59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.                 *
 ******************************************************************************/

#ifndef GRABBERCONTROLCAPABILITIES_H
#define GRABBERCONTROLCAPABILITIES_H


/**
 * Capabilities of a control of a device.
 *
 * @author Ralf Lange
 */
class GrabberControlCapabilities
{
public:

    /**
     * Device controller capabilities
     */
    enum grabberControlFlags {
        control_none,
        control_Auto,
        control_Manual,
        control_Relative,
        control_Absolute
    };

    /**
     * Constructs and initializes the object.
     */
    GrabberControlCapabilities();

    /**
     * Destructor
     */
    ~GrabberControlCapabilities();

    /**
     * Has the camera this control capability?
     * @return True if the camera has this control apability.
     */
    bool isCapability();

    /**
     * Has the capability a automatic control?
     * @return True if the capability has a automatic control.
     */
    bool isAutomatic();

    /**
     * Get the id of the control
     * @return The id of the control.
     */
    unsigned int getControlId();

    /**
     * Set the id of the control
     * @param i The new id of the control.
     */
    void setControlId(unsigned int cl);

    /**
     * Get the class of the control
     * @return The class of the control.
     */
    unsigned int getControlClass();

    /**
     * Set the class of the control
     * @param i The new class of the control.
     */
    void setControlClass(unsigned int id);

    /**
     * Get the type of the control
     * @return The type of the control.
     */
    unsigned int getControlType();

    /**
     * Set the type of the control
     * @param i The new type of the control.
     */
    void setControlType(unsigned int type);

    /**
     * Get the minimum value of the control
     * @return The minimum value of the control.
     */
    int getMinimum();

    /**
     * Set the minimum value of the control
     * @param mi The new minimum value of the control.
     */
    void setMinimum(int mi);

    /**
     * Get the maximum value of the control
     * @return The maximum value of the control.
     */
    int getMaximum();

    /**
     * Set the maximum value of the control
     * @param ma The new maximum value of the control.
     */
    void setMaximum(int ma);

    /**
     * Get the step value of the control
     * @return The step value of the control.
     */
    int getStep();

    /**
     * Set the step value of the control
     * @param st The new step value of the control.
     */
    void setStep(int st);

    /**
     * Get the default value of the control
     * @return The default value of the control.
     */
    int getDefault();

    /**
     * Set the default value of the control
     * @param fl The new default value of the control.
     */
    void setDefault(int de);

    /**
     * Get the flags value of the control
     * @return The flags value of the control.
     */
    long getFlags();

    /**
     * Set the flags value of the control
     * @param fl The new flags value of the control.
     */
    void setFlags(long fl);

private:

    unsigned int  ctrl_id;
    unsigned int  ctrl_class;
    unsigned int  ctrl_type;
    int           minimum;
    int           maximum;
    int           step;
    int           defaultt;
    long          flags;

};

#endif
